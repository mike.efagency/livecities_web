##   API DOCU

base_url: http://35.174.135.145:8000/v0


# HOME

* Search form: use this api call with the string provided by user in the input:
base_url + /search?q= + searchInputText
This action must lead to the search page so the api call can be fired there.

* Get 4 resources with this api call:
base_url + /search?class=task&size=4

Use a this image as placeholder since the API is not returning any pic url now:
https://placeimg.com/640/480/tech

Example response:
{
    "items": [
        {
            "model_class": "Resource",
            "id": 1,
            "name": "test resource",
            "description": "asgasgasg",
            "kind": "Unknown",
            "owner": {
                "id": 13,
                "name": "Tiyan"
            },
            "blocked": [],
            "followers_count": 0,
            "is_followed": false,
            "is_available": true
        },
        {
            "model_class": "Resource",
            "id": 6,
            "name": "This is a resource space (Place)",
            "description": "Lorem ipsum",
            "kind": "Place",
            "owner": {
                "id": 13,
                "name": "Tiyan"
            },
            "blocked": [],
            "followers_count": 0,
            "is_followed": false,
            "is_available": true
        }
    ],
    "total": 2,
    "filter": {
        "class": {
            "Resource": 2
        }
    }
}


* Get 9 projects with this api call:
base_url + /search?class=resource&kind=not-k&size=9

Use a this image as placeholder since the API is not returning any pic url now:
https://placeimg.com/640/480/tech

Example response:
{
    "items": [
        {
            "model_class": "Task",
            "id": 1,
            "name": "Task to test api",
            "description": "Lorem ipsum dolor est",
            "completed_count": 0,
            "tags": [
                "Music",
                "Culture"
            ],
            "status": "Public",
            "owner": {
                "id": 13,
                "name": "Tiyan"
            },
            "date": {
                "from": "2019-02-24T16:02:04",
                "to": "2019-02-28T16:02:05"
            },
            "followers_count": 0,
            "is_followed": false
        }
    ],
    "total": 1,
    "filter": {
        "class": {
            "Task": 1
        }
    }
}

##########




# PROFILE PAGE

* Use this action to send the new name/email of the user when it gets changed:
  actions/auth/updateProfile

param is send in the body as:
    email: {string}
    first_name: {string}
    last_name: {string}

Save the response in Redux to show the updated profile.

Example response:
{
    email: "newmail@mail.com",
    first_name: "John",
    last_name: "Doe"
}


* Use this action to send the whole profile information when user hit 'Save changes':
  actions/auth/updateProfileInfo

Split the string in the input with the commas:
Grado de Electronica, FP Mecanica ==> ["Grado de Electronica", "FP Mecanica"]

param is send in the body as:
    education: {array of strings}
    courses: {array of strings}
    schedule: {string}

Save the response in Redux to show the updated profile.

Example response:
{
    education: ["Grado de Electronica", "FP Mecanica"],
    courses: ["Testing with Jest"],
    schedule: "morning"
}


* Use this action to get the activities list:
  actions/activites/getActivities

Save the response in Redux to show the activities and be able to update them later.

Example response:
{
  key: 1,
  name: 'Curso de programación: testing unitario para',
  type: 'TALLER',
  target: 'adultos',
  price: 32,
  equipment: 'material',
  profile: 3
}


* Use this action to post a new activity when created with form::
  actions/activites/createActivity

param is send in the body as:
    name: {string}
    type: {string}
    target: {string}
    price: {float},
    equipment: {string},
    user_id: {int} // current user ID

Save the response in Redux to show the new activity.

Example response:
{
  key: 1,
  name: 'Curso de programación: testing unitario para',
  type: 'TALLER',
  target: 'adultos',
  price: 32,
  equipment: 'material',
  user_id: 3
}

##########




# CREATE PROJECT

* Use this api call to retrieve the list of the categories that will be in the dropdown:
base_url +  /tags/all

Example response:
"[{\"id\": 1, \"name\": \"Sports\"}, {\"id\": 2, \"name\": \"Entertainment\"}, {\"id\": 3, \"name\": \"Music\"}, {\"id\": 4, \"name\": \"Culture\"}]"


* After creating the project you will have and ID to post the selected tags, use this call within a loop to send each tag:
base_url + tasks/PK/tags PUT

in the body:
tag: string

Where PK is the ID of the project just created.


* To handle the activities list/create use the same calls and flows that are described in the Profile page.
ONLY difference is the way we post them to API. In this case you should store them until the moment the project is created and then use the project ID as in this example body:
    name: {string} // name of the activity
    type: {string} // type selected by user
    target: {string} // target selected by user
    price: {float}, // prive of the activity
    equipment: {string}, // selected by user
    project_id: {int} // current project ID

##########




# SPACE DETAIL PAGE

* Use this action to get the space info:
  actions/space/getSpace

Not sure if saving the response in Redux is needed here.

Example response:
{
    "id": 6,
    "created": "2019-02-15T00:18:34.220823",
    "name": "This is a resource space (Place)",
    "description": "Lorem ipsum",
    "owner": {
        "id": 13,
        "name": "Tiyan",
        "image": "https://placeimg.com/200/200/people"
    },
    "team": null,
    "isa": {
        "id": 16,
        "name": "This is a resource space",
        "description": "Lorem ipsum space is place",
        "score": 1,
        "kind": "P"
    },
    "latitude": 41.390205,
    "longitude": 2.154007,
    "owner_name": "Tiyan",
    "kind": "P",
    "is_available": true,
    "squaredfeet": 120,
    "price": 400,
    "capacity": 60,
    "equipment": ["wifi", "chairs", "toilets"],
    "images": [
        "https://placeimg.com/640/480/tech",
        "https://placeimg.com/640/480/tech",
        "https://placeimg.com/640/480/tech"
    ],
    "is_public": true,
    "blocked_dates": ["2019-03-14T12:00:00", "2017-03-15T12:00:00"],
    "followed_by_current_user": false,
    "feedbacks": [],
    "user_used_resource": false
}

Important notes:
_blocked_dates is used to mark this days in the calendar
_images is used to create a lightbox gallery
_show first image as jumbo bg


* Use this action when the user opens the modal form and hits ENVIAR:
  actions/space/sendMessageToOwner

param is send in the body as:
    user_id: {int}, // current user id
    resource_id: 2, // current space id
    dates: {array of dates string} // ex: ["2019-02-24T12:00:00", "2017-02-25T12:00:00"],
    message: {string} // the textarea input message

Save the response in Redux to show the updated profile.

Example response:
{
    user_id: 1,
    resource_id: 2,
    dates: ["2019-02-24T12:00:00", "2017-02-25T12:00:00"],
    message: "I would like to know if this space is available for those dates and blablabla"
}

##########




#   SEARCH PAGE

Important points to be aware of in this page:

* FILTERS: any change in any of those inputs must fire a new api call with those values as query params to refresh the results of the page.
As API don't support this yet, please do this logic and then show the results again. I want to see the api call fired + loading spinner + results back in place.


* LOAD MORE BUTTON: at the end of the page there is this button to load more results. We are going to use the pagination of the api. As API don't support this yet, please do this logic:
- User clicks on 'load more'
- Fire search api call
- Concat results to existing results on screen
- Refresh map if needed

THIS BOTH ACTIONS MUST UPDATE REDUX STORE PLEASE

##########





#   CAMBIOS

- Fechas van fuera de todas partes. Se pone en contacto por mail a los dueños e interesados y nada más.
- Los recursos siempre están disponibles, el dueño ya dirá si es así o no.


#   PANTALLAS QUE SE DEBERÍAN AÑADIR
- Ver los recursos y proyectos de un user
- Poder editar/eliminar un recurso/proyecto


#   REVISAR ERRORES

OSError at /v0/tasks/
[Errno 28] No space left on device

Request Method: PUT
Request URL: http://35.174.135.145:8000/v0/tasks/
Django Version: 1.11.18
Python Executable: /usr/local/bin/python3
Python Version: 3.6.4


#   DIEGO PENDING
1. /me (página: perfil de usuario)
Añadir algún campo tipo JSON donde yo parsear un array con info de:
- educación
- cursos
Estos son campos NO array
- horario disponible (string)
- distance (int)
Debe poder ser enviada/actualizada/leída PUT/POST/GET

2. Entidad `activities` (página: perfil de usuario, proyecto)
Nuevo iem que  tiene estos campos:
- name (string)
- type (enum) [taller/curso/concierto/actividad]
- target (enum) [niños/adolescentes/adultos/todos]
- price (int)
- equipment (boolean)
- user_id: (int) id del owner
Y cada user puede tener N y a su vez cada proyecto puede tener N
Es necesario poder crearlas, actualizarlas y leerlas
Además de poder enviar un correo a su dueño

3. Search que devuelva el campo image
http://35.174.135.145:8000/v0/search?class=resource&size=4


4. /me puede admitir POST? hay un formulario para cambiar el nombre y/o mail de usuario. Si no son más de 20min adelante
