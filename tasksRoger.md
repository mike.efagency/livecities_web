## TASKS FOR ROGER

1. On signup_form.js
Use the api call below to send the list of skills:

const AddMeKnowledge = function (token, skill) {
    const skillUrl = server_url + '/me/knowledge';
    var data = {
        name: skill  // Better use id than name
    };

    return fetch(skillUrl, {
        method: 'PUT',
        headers: {
            'authorization': token,
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
    }).then(resp => resp.json());
}
