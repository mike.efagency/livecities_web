import React, { Component } from 'react';
import { Provider } from 'react-redux';
import {NotificationContainer} from 'react-notifications';
import { PersistGate } from 'redux-persist/integration/react'

import { store, persistor } from './store';
import Router from './containers/Routes';
import 'react-notifications/lib/notifications.css';
import 'react-circular-progressbar/dist/styles.css';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';
import './App.css';


class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <Router />
          <NotificationContainer />
        </PersistGate>
      </Provider>
    );
  }
}

export default App;
