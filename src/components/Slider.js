import * as React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Slider from '@material-ui/lab/Slider';
import { SText, SBView } from '../styles/global';
import { Colors } from '../lib/theme';


const styles = {
  slider: {
    padding: '20px 10px'
  },
  thumbIconWrapper: {
    width: 30,
    height: 30,
    backgroundColor: Colors.green,
    borderRadius: 30,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  track: {
    backgroundColor: Colors.green,
  },
  label: {
    fontSize: 12,
    color: Colors.text
  }
};

class CustomSlider extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: props.value
    }
  }

  handleChange = name => event => {
    this.setState({ [name]: event.target.checked });
    this.props.onChange(event.target.checked);
  };

  render() {
    const { classes, value, maxValue, step, label, unit } = this.props;
    return (
      <div>
        <SText>{label}</SText>
        <Slider
          classes={{ 
            container: classes.slider,
            track: classes.track,
            thumb: classes.thumbIconWrapper
          }}
          value={value}
          min={0}
          max={maxValue}
          step={step}
          onChange={(event, value) => this.props.onChange(value)}
          thumb={
            <div className={classes.thumbIconWrapper}>
              <span>{value}</span>
            </div>
          }
        />
        <SBView>
          <span className={classes.label}>0 {unit}</span>
          <span className={classes.label}>{maxValue} {unit}</span>
        </SBView>
      </div>
    );
  }
}

CustomSlider.propTypes = {
  label: PropTypes.string.isRequired,
  value: PropTypes.number.isRequired,
  maxValue: PropTypes.number.isRequired,
  unit: PropTypes.string,
  step: PropTypes.number,
  onChange: PropTypes.func.isRequired
};

CustomSlider.defaultProps = {
  unit: 'Km',
  step: 1
}

export default withStyles(styles)(CustomSlider);
