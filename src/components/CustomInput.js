import * as React from 'react';
import { withStyles } from '@material-ui/core/styles';

import PropTypes from 'prop-types';

import { FormControl, Icon, InputBase, NativeSelect } from '@material-ui/core';

import { Colors } from '../lib/theme';

import IfView from './IfView';

import { WhiteMText } from '../styles/global';

const styles = theme => ({
  formControl: {
    width: '100%'
  },
  select: {
    width: 'calc(100% - 24px)'
  },
  bootstrapInput: {
    borderRadius: 4,
    position: 'relative',
    backgroundColor: theme.palette.common.white,
    border: '1px solid #ced4da',
    fontSize: 16,
    padding: '10px 12px',
    transition: theme.transitions.create(['border-color', 'box-shadow']),
    '&:focus': {
      borderRadius: 4,
      borderColor: '#80bdff',
      boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
      backgroundColor: 'white'
    },
  },
  rightIcon: {
    top: 'calc(50% - 15px)',
    right: 10,
    color: Colors.green,
    position: 'absolute',
    fontSize: 30
  }
})


class CustomInput extends React.Component {

  icon = () => {
    return(
      <Icon className={this.props.classes.rightIcon}>keyboard_arrow_down</Icon>
    )
  }

  render() {

    const { classes, label, value, options, type, color, disableFirst } = this.props;

    return(
      <FormControl className={classes.formControl}>

        {label.length > 0 && <WhiteMText>&nbsp;&nbsp;{label}</WhiteMText>}

        <IfView condition={type==="input"}>
          <InputBase
            id="input"
            error={true}
            onChange={this.props.onChange}
            onBlur={this.props.onBlur}
            value={value}
            classes={{ input: classes.bootstrapInput }}
          />

          <NativeSelect
            classes={{ select: classes.select }}
            IconComponent={this.icon}
            input={
              <InputBase
                id="type"
                error={true}
                onChange={this.props.onChange}
                value={value}
                classes={{ input: classes.bootstrapInput }}
                style={{color}}
              />
            }
          >
            {
              options.map((option, index) => (
                <option
                  disabled={disableFirst && index === 0 ? true : false}
                  key={option}
                  value={option}
                >
                  {option}
                </option>
              ))
            }
          </NativeSelect>
        </IfView>
      </FormControl>
    )
  }
}

CustomInput.propTypes = {
  label: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  onBlur: PropTypes.func,
  options: PropTypes.array,
  type: PropTypes.string, // input or select,
  color: PropTypes.string,
  disableFirst: PropTypes.bool
}

CustomInput.defaultProps = {
  options: [],
  type: 'input',
  color: Colors.text,
  disableFirst: false
}

export default withStyles(styles)(CustomInput);
