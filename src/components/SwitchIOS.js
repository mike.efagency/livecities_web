import * as React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Switch from '@material-ui/core/Switch';

const styles = theme => ({
  iOSSwitchBase: {
    '&$iOSChecked': {
      color: theme.palette.common.white,
      '& + $iOSBar': {
        backgroundColor: '#52d869',
      },
    },
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
      easing: theme.transitions.easing.sharp,
    }),
  },
  iOSBar: {
    borderRadius: 13,
    width: 42,
    height: 26,
    marginTop: -13,
    marginLeft: -21,
    border: 'solid 1px',
    borderColor: theme.palette.grey[400],
    backgroundColor: theme.palette.grey[50],
    opacity: 1,
    transition: theme.transitions.create(['background-color', 'border']),
  },
  iOSIcon: {
    width: 24,
    height: 24,
  },
  iOSIconChecked: {
    boxShadow: theme.shadows[1],
  },
  iOSChecked: {
    transform: 'translateX(15px)',
    '& + $iOSBar': {
      opacity: 1,
      border: 'none',
    },
  },
});

class SwitchIOS extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: props.value
    }
  }

  handleChange = name => event => {
    this.setState({ [name]: event.target.checked });
    this.props.onChange(event.target.checked);
  };

  render() {
    const { classes } = this.props;
    return (
      <Switch
        classes={{
          switchBase: classes.iOSSwitchBase,
          bar: classes.iOSBar,
          icon: classes.iOSIcon,
          iconChecked: classes.iOSIconChecked,
          checked: classes.iOSChecked,
        }}
        disableRipple
        checked={this.state.checkedB}
        onChange={this.handleChange('checked')}
        value="checkedB"
      />
    );
  }
}

SwitchIOS.propTypes = {
  value: PropTypes.bool,
  onChange: PropTypes.func.isRequired
};

SwitchIOS.defaultProps = {
  value: false
}

export default withStyles(styles)(SwitchIOS);
