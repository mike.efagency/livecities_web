import * as React from 'react';
import { withStyles } from '@material-ui/core/styles';

import PropTypes from 'prop-types';

import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';

import OrangeButton  from './OrangeButton';

const styles = theme => ({
  container: {
    display: 'flex',
    alignItems: 'center',
    height: 60,
    borderRadius: 10,
    backgroundColor: 'white',
    marginTop: 20,
    position: 'relative',
    paddingLeft: 20,
    paddingRight: 20,
    border: '1px solid rgba(0, 0, 0, 0.23)',
    [theme.breakpoints.down('sm')]: {
      paddingLeft: 10,
      paddingRight: 10,
    },
  },
  input: {
    flex: 1,
    outline: 0,
    height: 50,
    border: 0,
    fontSize: 24,
    marginLeft: 10,
    marginRight: 10,
    [theme.breakpoints.down('sm')]: {
      fontSize: 16,
    },
  }
});


class SearchInput extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      input: ''
    }
  }


  render() {

    const { classes, placeholder, value } = this.props;

    return (
      <div className={classes.container}>
        <SearchIcon />
        <InputBase
          className={classes.input}
          value={value}
          placeholder={placeholder}
          onChange={this.props.onChange}
        />
        <OrangeButton text="Buscar" onPress={() => this.props.onSearch()}/>
      </div>
    );
  }
}

SearchInput.propTypes = {
  placeholder: PropTypes.string.isRequired,
  onSearch: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired
};

SearchInput.defaultProps = {

}

export default withStyles(styles)(SearchInput);
