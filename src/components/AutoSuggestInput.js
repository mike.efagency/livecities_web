import * as React from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';

import PropTypes from 'prop-types';

import { MenuItem, Paper, Popper, TextField } from '@material-ui/core';

import Autosuggest from 'react-autosuggest';
import match from 'autosuggest-highlight/match';
import parse from 'autosuggest-highlight/parse';
import IfView from './IfView';

import { Bold, GrayMText } from '../styles/global';

const styles = {
  suggestion: {
    display: 'block',
  },
  suggestionsList: {
    margin: 0,
    padding: 0,
    listStyleType: 'none',
  },
};


class AutoSuggestInput extends React.Component {

  constructor(props) {

    super(props);

    this.state = {
      single: '',
      popper: '',
      suggestions: []
    };
  }


  // Use skills autocomplete API call to show the possible skills
  handleSuggestionsFetchRequested = ({ value }) => {

    let cb = results => {

        this.setState({
          suggestions: results,
        });
      };

    this.props.getSkills(value, cb)
  }

  // Clear suggestions
  handleSuggestionsClearRequested = () => {

    this.setState({
      suggestions: [],
    });
  }

  // New value present in the input
  handleChange = (event, {newValue}) => {

    this.props.onChange(newValue);
  }

  // Only trigger fetch suggestions when input is longer than 2 chars
  handleShouldRenderSuggestions = value => {

    return value.trim().length > 2;
  }

  renderInputComponent = (inputProps) => {

    const { classes, inputRef = () => {}, ref, ...other } = inputProps;

    return (
      <TextField
        fullWidth
        id="outlined-name"
        label="Email"
        variant="outlined"
        className={classes.textField}
        InputProps={{
          inputRef: node => {
            ref(node);
            inputRef(node);
          },
          classes: {
            input: classes.input,
          },
        }}
        {...other}
        onKeyPress={(ev) => {
          if (ev.key === 'Enter') {
            // Do code here
            this.props.onSelect(this.props.value);
            this.props.onChange('');
            ev.preventDefault();
          }
        }}
      />
    );
  }

  renderSuggestion = (suggestion, { query, isHighlighted }) => {

    const matches = match(suggestion.name, query);

    const parts = parse(suggestion.name, matches);

    return (
      <MenuItem
        selected={isHighlighted}
        component="div"
        onClick={() => this.onClickSuggestion(suggestion.name)}
      >
          {
            parts.map((part, index) => (
              <IfView key={String(index)} condition={part.highlight}>
                <Bold><GrayMText>{part.text}</GrayMText></Bold>
                <GrayMText>{part.text}</GrayMText>
              </IfView>
            ))
          }
      </MenuItem>
    );
  }

  onClickSuggestion = (text) => {

    this.props.onSelect(text);

    setTimeout(() => {
      this.props.onChange('');
    })
  }

  getSuggestionValue = (suggestion) => {

    return suggestion.name;
  }

  render() {

    const { classes, placeholder, label, value } = this.props;

    const autosuggestProps = {
      renderInputComponent: this.renderInputComponent,
      suggestions: this.state.suggestions,
      onSuggestionsFetchRequested: this.handleSuggestionsFetchRequested,
      onSuggestionsClearRequested: this.handleSuggestionsClearRequested,
      getSuggestionValue: this.getSuggestionValue,
      renderSuggestion: this.renderSuggestion,
      shouldRenderSuggestions: this.handleShouldRenderSuggestions
    };

    return (

      <div style={{marginTop: 20}}>
        <Autosuggest
          {...autosuggestProps}
          inputProps={{
            classes,
            label: label,
            color: 'black',
            placeholder,
            value,
            onChange: this.handleChange,
            inputRef: node => {
              this.popperNode = node;
            },
            InputLabelProps: {
              shrink: true,
            },
          }}
          theme={{
            suggestionsList: classes.suggestionsList,
            suggestion: classes.suggestion,
          }}
          renderSuggestionsContainer={options => (
            <Popper anchorEl={this.popperNode} open={Boolean(options.children)}>
              <Paper
                square
                {...options.containerProps}
                style={{ width: this.popperNode ? this.popperNode.clientWidth : null }}
              >
                {options.children}
              </Paper>
            </Popper>
          )}
        />
      </div>
    );
  }
}

AutoSuggestInput.propTypes = {
  classes: PropTypes.object.isRequired,
  value: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  selectedValues: PropTypes.array,
  onChange: PropTypes.func.isRequired,
  onSelect: PropTypes.func.isRequired,
  getSkills: PropTypes.func.isRequired
};

AutoSuggestInput.defaultProps = {
  selectedValues: []
}

export default withStyles(styles)(connect(null, {})(AutoSuggestInput));
