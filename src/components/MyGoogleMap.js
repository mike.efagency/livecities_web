import * as React from 'react';
import { compose, withProps } from "recompose"
import { withGoogleMap, GoogleMap } from "react-google-maps"

export const MyGoogleMap = compose(
  withProps({
    googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyA9ubLXMECtXi8WQCt-UFZeSNiJbdKhOaM&v=3.exp&libraries=geometry,drawing,places",
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `90%` }} />,
    mapElement: <div style={{ height: `100%` }} />,
  }),
  withGoogleMap,
)((props) => (
  <GoogleMap
    zoom={props.zoom}
    defaultCenter={props.center}
  >
    {props.children}
  </GoogleMap>
))
