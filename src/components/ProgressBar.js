import * as React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import { Colors } from '../lib/theme';

const styles = {
  container: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  barWrapper: {
    borderRadius: 8,
    background: 'lightgray',
    overflow: 'hidden',
    position: 'relative'
  },
  progressText: {
    color: Colors.green
  },
  progressFilled: {
    backgroundColor: Colors.green,
    height: 8,
    borderRadius: 8,
    position: 'absolute',
    left: 0,
    top: 0
  }
}

class ProgressBar extends React.Component {

  render() {
    const { classes, value, width, showValue, height, fullWidth } = this.props;
    return (
      <div className={classes.container}>
        {showValue && <center className={classes.progressText}>{value}%</center>}
        <div className={classes.barWrapper} style={{width: fullWidth ? '100%' : width, height}}>
          <div className={classes.progressFilled} style={{width: value + '%', height}} />
        </div>
      </div>      
    );
  }
}

ProgressBar.propTypes = {
  value: PropTypes.number.isRequired,
  width: PropTypes.number,
  showValue: PropTypes.bool,
  height: PropTypes.number,
  fullWidth: PropTypes.bool
};

ProgressBar.defaultProps = {
  width: 100,
  showValue: true,
  height: 8,
  fullWidth: false
}

export default withStyles(styles)(ProgressBar);
