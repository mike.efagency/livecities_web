import * as React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import { Colors } from '../lib/theme';

const styles = theme => ({
  button: {
    backgroundColor: Colors.orange,
    color: 'white',
    height: 40,
    [theme.breakpoints.down('sm')]: {
      padding: '0 10px'
    },
    [theme.breakpoints.up('sm')]: {
      padding: '0 20px'
    },
  },
  progressBar: {
    color: 'white',
    marginLeft: 20
  }
});

class OrangeButton extends React.Component {

  render() {
    const { classes, text, loading } = this.props;
    return (
        <Button 
          variant="contained" 
          color="primary" 
          className={classes.button}
          onClick={() => this.props.onPress()}
        >
          <span>{text}</span> 
          {loading && <CircularProgress className={classes.progressBar} size={16}/>}
        </Button>
    );
  }
}

OrangeButton.propTypes = {
  text: PropTypes.string.isRequired,
  paddingHorizontal: PropTypes.number,
  onPress: PropTypes.func.isRequired,
  loading: PropTypes.bool
};

OrangeButton.defaultProps = {
  paddingHorizontal: 20,
  loading: false
}

export default withStyles(styles)(OrangeButton);
