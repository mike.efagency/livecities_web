import * as React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { withStyles } from '@material-ui/core/styles';

import Grid from '@material-ui/core/Grid';

import { screenActions } from '../redux/actions';
import { whiteLogo, appStoreLogo, googleStoreLogo } from '../lib/images';
import { Colors } from '../lib/theme';

import { WhiteMText, WhiteSText, WhiteLText, Bold } from '../styles/global';
import { Container, StoreIcon, BottomLogo } from '../styles/footer';

const styles = theme => ({
  container: {
    backgroundColor: Colors.blue
  },
  sectionView: {
    [theme.breakpoints.down('md')]: {
      padding: 30,
      paddingLeft: 60
    },
    [theme.breakpoints.up('md')]: {
      padding: 20,
    },
  },
  sectionText: {
    color: Colors.lightgray,
    textDecoration: 'none',
    fontSize: 14,
    '&:hover': {
      textDecoration: 'underline',
    },
  },
});


class Footer extends React.Component {

  componentDidMount() {

    this.updateWindowDimensions();

    window.addEventListener('resize', this.updateWindowDimensions);


    // TODO: check if this is needed
    // Auto redirect for unlogged users
    // const { loggedIn, location: {pathname} } = this.props;
    //
    // if (!loggedIn && pathname.indexOf('signup') < 0) {
    //   this.props.history.push('/login');
    // }
  }

  componentWillUnmount() {

    window.removeEventListener('resize', this.updateWindowDimensions);
  }

  updateWindowDimensions = () => {

    this.props.setDimension({
      width: window.innerWidth,
      height: window.innerHeight
    });
  }


  render() {

    const { classes } = this.props;

    return (
      <Container>
        <Grid container justify="center">
          <Grid item xs={12} md={10}>
            <Grid container>
              <Grid item xs={12} md={3} className={classes.sectionView}>
                <WhiteMText>LiveCities</WhiteMText>
                <WhiteSText><a href="http://www.8wires.io" className={classes.sectionText}>Home</a></WhiteSText>
                <WhiteSText><a href="http://www.8wires.io" className={classes.sectionText}>Tour</a></WhiteSText>
                <WhiteSText><a href="http://www.8wires.io" className={classes.sectionText}>Pricing</a></WhiteSText>
                <WhiteSText><a href="http://www.8wires.io" className={classes.sectionText}>Add-Ons</a></WhiteSText>
                <WhiteSText><a href="http://www.8wires.io" className={classes.sectionText}>Security</a></WhiteSText>
                <WhiteSText><a href="http://www.8wires.io" className={classes.sectionText}>Button & Widget</a></WhiteSText>
              </Grid>
              <Grid item xs={12} md={3} className={classes.sectionView}>
                <WhiteMText>Community</WhiteMText>
                <WhiteSText><a href="http://www.8wires.io" className={classes.sectionText}>Customers</a></WhiteSText>
                <WhiteSText><a href="http://www.8wires.io" className={classes.sectionText}>Webinars</a></WhiteSText>
                <WhiteSText><a href="http://www.8wires.io" className={classes.sectionText}>Help & Support</a></WhiteSText>
                <WhiteSText><a href="http://www.8wires.io" className={classes.sectionText}>Blog</a></WhiteSText>
                <WhiteSText><a href="http://www.8wires.io" className={classes.sectionText}>Resources</a></WhiteSText>
                <WhiteSText><a href="http://www.8wires.io" className={classes.sectionText}>API</a></WhiteSText>
              </Grid>
              <Grid item xs={12} md={3} className={classes.sectionView}>
                <WhiteMText>Company</WhiteMText>
                <WhiteSText><a href="http://www.8wires.io" className={classes.sectionText}>About Us</a></WhiteSText>
                <WhiteSText><a href="http://www.8wires.io" className={classes.sectionText}>Made on Earth</a></WhiteSText>
                <WhiteSText><a href="http://www.8wires.io" className={classes.sectionText}>Press</a></WhiteSText>
                <WhiteSText><a href="http://www.8wires.io" className={classes.sectionText}>Careers</a></WhiteSText>
                <WhiteSText><a href="http://www.8wires.io" className={classes.sectionText}>Legal</a></WhiteSText>
                <WhiteSText><a href="http://www.8wires.io" className={classes.sectionText}>Contact Us</a></WhiteSText>
              </Grid>
              <Grid item xs={12} md={3} className={classes.sectionView}>
                <WhiteLText><span><img src={whiteLogo} alt="LiveCities logo"/></span><Bold>LIVE</Bold>CITIES</WhiteLText>
                <div><StoreIcon src={googleStoreLogo} /></div>
                <div><StoreIcon src={appStoreLogo} /></div>
              </Grid>
            </Grid>
          </Grid>
          <Grid container justify="center">
              <BottomLogo>© 2018 - 8wires</BottomLogo>
          </Grid>
        </Grid>
      </Container>
    );
  }
}


const mapStateToProps = (state) => ({
  loggedIn: state.userReducer.loggedIn
});

const mapDispatchToProps = ({
  setDimension: screenActions.setDimension
})

export default withStyles(styles)(withRouter(connect(mapStateToProps, mapDispatchToProps)(Footer)));
