import * as React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { withStyles } from '@material-ui/core/styles';

import PropTypes from 'prop-types';

import { AppBar, Button, Divider, Drawer, Grid, IconButton, List, ListItem, ListItemText } from '@material-ui/core';
import MoreVertIcon from '@material-ui/icons/MoreVert';

import { authActions } from '../redux/actions';
import { headerLogo } from '../lib/images';

import { LogoImage } from '../styles/header';
import { LText, Bold } from '../styles/global';

const styles = theme => ({
  appBar: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    width: 'auto'
  },
  headerView: {
    height: 80
  },
  logo: {
    width: 40,
    height: 40,
    marginRight: 20
  },
  logoView: {
    display: 'flex',
    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    '&:hover': {
      background: 'none'
    }
  },
  logoText: {
    color: 'black'
  },
  buttonView: {
    display: 'flex',
    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  button: {
    marginRight: 20
  },
  registerButton: {
    marginRight: 20,
    height: 40,
    borderRadius: 20,
    border: '1px solid green'
  },
  logoutButton: {
    borderRadius: 20,
    border: '1px solid red'
  },
  mobileHeaderView: {
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
    [theme.breakpoints.down('sm')]: {
      display: 'flex',
    },
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 20,
    paddingRight: 20
  },
  mobileHeaderLogoView: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  }
});


class Header extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      anchorEl: null,
      topDrawer: false
    }
  }


  handleClickDrawerOption = index => () => {

    const { loggedIn, history } = this.props;

    switch (index) {
      case 0:
        history.push('/search?category=resource');
        break;

      case 1:
        history.push('/search?category=project');
        break;

      case 2:
        if (loggedIn) {
          history.push('/myprofile');
        } else {
          history.push('/login');
        }
        break;

      case 3:
        history.push('/signup');
        break;

      default:
        break;
    }
  }

  logOut = () => {

    this.props.logOut();

    this.props.history.push('/login');
  }

  renderMobileMenuList = () => {

    const { loggedIn, classes } = this.props;

    const menuList = loggedIn
      ? ['Publica un recurso', 'Crea un proyecto', 'Mi perfil']
      : ['Publica un recurso', 'Crea un proyecto', 'Inicia Sesión', 'Registrate'];

    return (
      <div className={classes.fullList}>
        <List>
          {menuList.map((text, index) => (
            <ListItem button key={index} onClick={this.handleClickDrawerOption(index)}>
              <ListItemText primary={text} />
            </ListItem>
          ))}
        </List>

        {loggedIn && <Divider />}
        {loggedIn &&
          <ListItem button key={menuList.length} onClick={() => this.logOut()}>
            <ListItemText primary="Log Out" />
          </ListItem>
        }
      </div>
    )
  }

  toggleDrawer = (side, open) => () => {
    this.setState({
      [side]: open,
    });
  };


  render() {

    const { classes, background, loggedIn } = this.props;

    const { anchorEl } = this.state;

    const open = Boolean(anchorEl),
      colorStyle = {
        color: background === 'transparent' ? 'white' : 'black'
      };

    return (
      <AppBar style={{ background, boxShadow: background === 'transparent' ? 'none' : null}}>
        <Grid container>
          <Grid item sm={12} md={5} className={classes.logoView}>
            <Button className={classes.logoView} href={loggedIn ? "/home" : "#"}>
              <LogoImage src={headerLogo}/>
              <LText style={{margin: 10}}><Bold>LIVE</Bold>CITIES</LText>
            </Button>
          </Grid>
          {
            loggedIn ?
            <Grid item sm={12} md={7} className={classes.buttonView}>
              <Button className={classes.button} href="/resource/new" style={colorStyle}>Publica un recurso</Button>
              <Button className={classes.button} href="/project/new" style={colorStyle}>Crea un proyecto</Button>
              <Button variant="outlined" className={classes.registerButton} href="/myprofile/" style={colorStyle}>Mi perfil</Button>
              <Button variant="outlined" className={classes.logoutButton} onClick={() => this.logOut()} style={colorStyle}>Cerrar sesión</Button>
            </Grid>
            :
            <Grid item sm={12} md={7} className={classes.buttonView}>
              <Button className={classes.button} href="/login" style={colorStyle}>Publica un recurso</Button>
              <Button className={classes.button} href="/login" style={colorStyle}>Crea un proyecto</Button>
              <Button className={classes.button} href="/login" style={colorStyle}>Inicia Sesión</Button>
              <Button variant="outlined" className={classes.registerButton} href="/signup" style={colorStyle}>Regístrate</Button>
            </Grid>
          }
          <Grid item xs={12}>
            <div className={classes.mobileHeaderView}>
              <Button className={classes.mobileHeaderLogoView} href={loggedIn ? "/home" : "#"}>
                <LogoImage src={headerLogo} />
                <LText style={{margin: 10}}><Bold>LIVE</Bold>CITIES</LText>
              </Button>
              <IconButton
                aria-label="More"
                aria-owns={open ? 'long-menu' : undefined}
                aria-haspopup="true"
                onClick={this.toggleDrawer('topDrawer', true)}
              >
                <MoreVertIcon />
              </IconButton>
              <Drawer anchor="top" open={this.state.topDrawer} onClose={this.toggleDrawer('topDrawer', false)}>
                <div
                  tabIndex={0}
                  onClick={this.toggleDrawer('topDrawer', false)}
                  onKeyDown={this.toggleDrawer('topDrawer', false)}
                >
                  {this.renderMobileMenuList()}
                </div>
              </Drawer>
            </div>
          </Grid>
        </Grid>
      </AppBar>
    );
  }
}

Header.propTypes = {
  background: PropTypes.string,
};

Header.defaultProps = {
  background: 'transparent'
}


const mapStateToProps = (state) => ({
  loggedIn: state.userReducer.loggedIn
});

const mapDispatchToProps = ({
  logOut: authActions.logOut
})

export default withStyles(styles)(withRouter(connect(mapStateToProps, mapDispatchToProps)(Header)));
