import AddressPicker from './AddressPicker';
import AutoCompleteInput from './AutoCompleteInput';
import AutoSuggestInput from './AutoSuggestInput';
import CustomInput from './CustomInput';
import CustomDatePicker from './DatePicker';
import CustomSlider from './Slider';
import Footer from './Footer';
import Header from './Header';
import IfView from './IfView';
import OrangeButton from './OrangeButton';
import ProgressBar from './ProgressBar'
import SearchInput from './SearchInput';
import SwitchIOS from './SwitchIOS';

export {
  AddressPicker,
  AutoCompleteInput,
  AutoSuggestInput,
  CustomInput,
  CustomDatePicker,
  CustomSlider,
  Footer,
  Header,
  IfView,
  OrangeButton,
  ProgressBar,
  SearchInput,
  SwitchIOS,
}
