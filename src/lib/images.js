
export const headerLogo = require('../assets/images/header_logo.png');
export const whiteLogo = require('../assets/images/white_logo.png');
export const welcomeBackground = require('../assets/images/welcome_back.jpg');
export const avatar = require('../assets/images/avatar.jpg');
export const defaultAvatar = require('../assets/images/default_avatar.png');

// APP STORES
export const appStoreLogo = require('../assets/images/appstores/app_store.png');
export const googleStoreLogo = require('../assets/images/appstores/google_store.png');

// ICONS
export const surfaceIcon = require('../assets/images/icons/react_icon.png');
export const capacityIcon = require('../assets/images/icons/capacity_icon.png');
export const costIcon = require('../assets/images/icons/cost_icon.png');

// STOCK IMAGES
export const background1 = require('../assets/images/background/1.jpg');
export const background2 = require('../assets/images/background/2.jpg');
export const background9 = require('../assets/images/background/9.jpg');
