
export const validateEmail = email => {

  /* eslint-disable-next-line no-useless-escape */
  var re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

  return re.test(String(email));
}

export const validatePassword = password => {

  let re = /((?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20})/;

  return re.test(String(password));
}

export const validatePrice = price => {

  let re = /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/;

  return re.test(String(price));
}

export const getPopularResources = resources => {

  return resources.sort(compareObjectValues('average_rating')).slice(0, 4)
}

export const compareObjectValues = (key, order = 'asc') => {

  return function (a, b) {

    if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
      // property doesn't exist on either object
      return 0;
    }

    const varA = (typeof a[key] === 'string') ?
      a[key].toUpperCase() : a[key];

    const varB = (typeof b[key] === 'string') ?
      b[key].toUpperCase() : b[key];

    let comparison = 0;

    if (varA > varB) {
      comparison = 1;
    } else if (varA < varB) {
      comparison = -1;
    }

    return (
      (order === 'desc') ? (comparison * -1) : comparison
    );
  };
}

export const getRatingCounts = ratingData => {

  return ratingData.reduce((total, obj) => (total + obj.count), 0);
}

/**
 * Safe apply of callback functions
 *
 * @param  {Function} cb to be executed
 * @param  {int} timeoutValue in miliseconds
 * @return {void} cb gets executed after timeoutValue ms
 */
export const apply = (cb, timeoutValue = 150) => {
  setTimeout(cb, timeoutValue);
}

/**
 * Used to validate the fields in user signup form
 * error object example:
 * {
 *  'name': 'This field is required',
 *  'email': 'Enter a valid email'
 * }
 *
 * @param  {string} key        name of the field
 * @param  {string} inputValue its value
 * @param  {object} error      current error object
 *
 * @return {object}            new error object
 */
export const checkSignupFields = (key, inputValue, error) => {

  if (inputValue.length === 0) {
    error[key] = 'This field is required';

  } else {

    delete error[key];
  }


  if (key === 'accepted') {

    if (inputValue) {

      delete error[key];

    } else {
      error[key] = `Terms and conditions must be agreed before creating a new account`
    }
  }

  if (key === 'password' && inputValue.length > 0) {

    // check also that password is valid
    if (validatePassword(inputValue)) {
      delete error[key];

    } else {
      error[key] = `Password must have between 8 and 20 characters among: numbers, lowercase and uppercase letters`;
    }
  }

  if (key === 'email' && inputValue.length > 0) {

    if (validateEmail(inputValue)) {
      delete error[key];

    } else {
      error[key] = `Enter a valid email`;
    }
  }

  return error;
}

/**
 * Used to shuffle a given array

 * @param  {array} array to be randomized

 * @return {array} the same array but shuffled
 */
export const shuffle = array => {

  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }

  return array
}

/**
 * Used to get a hashcode with a string seed
 *
 * @param  {string} str seed
 *
 * @return {int} generated hashcode, 10 digits
 */
export const getHash = str => {

  let hash = 0,
    i,
    chr;

  if (str.length === 0)
    return hash;

  for (i = 0; i < str.length; i++) {
    chr = str.charCodeAt(i);

    hash = ((hash << 5) - hash) + chr;

    // Convert to 32bit integer
    hash |= 0;
  }

  return hash
}
