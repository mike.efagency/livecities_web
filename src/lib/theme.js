export const Colors = {
  green: '#4EBD97',
  orange: '#F44E21',
  blue: '#0F385E',
  error: '#f44336',
  gray: '#555555',
  lightgray: '#999999',
  text: '#222222'
}