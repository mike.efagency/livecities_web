
export const GoogleMapKey = "AIzaSyA9ubLXMECtXi8WQCt-UFZeSNiJbdKhOaM";
export const ApiKey = 'AIzaSyAeNVbPAcqPJS2VJ6R7G3lKvUi52-MU3LQ';

// FORM HARCODED OPTIONS

export const ProfileSchedule = [
  'Mañana',
  'Mediodía',
  'Tarde',
  'Noche'
]

export const ActivityTypes = [
  'Curso',
  'Taller',
  'Concierto',
  'Proyección',
  'Coloquio',
  'Otro',
]

export const ActivityTargets = [
  'Todas las edades',
  'Niños',
  'Adolescentes',
  'Adultos',
]

export const ActivityEquipments = [
  'No',
  'Sí',
  'No es necesario',
]


// MOCKUP DATA

export const Activities__exampleData = [
  {
    key: 1,
    name: 'Curso de programación: testing unitario para',
    type: 'Curso',
    target: 'Todas las edades',
    price: '32',
    materialsIncluded: 'No'
  },
  {
    key: 2,
    name: 'Curso de programación: testing unitario para',
    type: 'Taller',
    target: 'Niños',
    price: '11',
    materialsIncluded: 'Sí',
  },
  {
    key: 3,
    name: 'Curso de programación: testing unitario para',
    type: 'Concierto',
    target: 'Adultos',
    price: '54',
    materialsIncluded: 'No es necesario',
  }
]

export const UserInformation__exampleData = {
  education: ["Grado de Electronica", "FP Mecanica"],
  courses: ["Testing with Jest"],
  schedule: "morning"
}

export const Space__exampleData = {
  "id": 6,
  "created": "2019-02-15T00:18:34.220823",
  "name": "This is a resource space (Place)",
  "description": "Lorem ipsum",
  "owner": {
    "id": 13,
    "name": "Tiyan",
    "image": "https://placeimg.com/200/200/people"
  },
  "team": null,
  "isa": {
    "id": 16,
    "name": "This is a resource space",
    "description": "Lorem ipsum space is place",
    "score": 1,
    "kind": "P"
  },
  "latitude": 41.390205,
  "longitude": 2.154007,
  "owner_name": "Tiyan",
  "kind": "P",
  "is_available": true,
  "squaredfeet": 120,
  "price": 400,
  "capacity": 60,
  "equipment": ["wifi", "chairs", "toilets"],
  "images": [
    "https://placeimg.com/640/480/tech",
    "https://placeimg.com/640/480/tech",
    "https://placeimg.com/640/480/tech"
  ],
  "is_public": true,
  "blocked_dates": ["2019-03-14T12:00:00", "2019-03-15T12:00:00"],
  "followed_by_current_user": false,
  "feedbacks": [],
  "user_used_resource": false
}

export const Space__equipmentList = [
  {name: 'toilets'},
  {name: 'wifi'},
  {name: 'meeting rooms'}
]
