import * as React from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';

import { NotificationManager } from 'react-notifications';

import PropTypes from 'prop-types';

import { Avatar, Button, Chip, Divider, Fab, FormControlLabel, Grid, Icon, Radio, RadioGroup, TextField } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';

import { AutoSuggestInput, CustomSlider, OrangeButton, ProgressBar } from '../../components';
import { authActions, activityActions } from '../../redux/actions';
import { Activities__exampleData, ProfileSchedule } from '../../lib/constants';
import { Colors } from '../../lib/theme';

import NewActivity from './new_activity';
import EditDialog from './edit_modal';

import { Bold, ErrorText, WhiteMText, WhiteLText, RightView, MText, GraySText, WrapView, LightGraySText, WhiteSText } from '../../styles/global';
import { AvatarSection, ActivityCard, AddActivityCard } from '../../styles/profile';

const styles = theme => ({
  avatar: {
    width: 100,
    height: 100,
    borderRadius: 100
  },
  editIcon: {
    width: 36,
    height: 36,
  },
  textField: {
    width: '100%'
  },
  fileInput: {
    width: 0,
    height: 0
  },
  radioGroup: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 30,
    paddingRight: 30,
    paddingBottom: 30
  },
  radio: {
    width: 12,
    height: 12,
    borderRadius: 24,
    border: '6px solid ' + Colors.green
  },
  activityPriceView: {
    [theme.breakpoints.up('sm')]: {
      display: 'flex',
      justifyContent: 'space-between'
    },
  },
  addActivityIcon: {
    fontSize: 50,
    color: Colors.green
  },
  ActivityCardButton: {
    width: '100%'
  },
  tagChip: {
    margin: 5
  }
})


class Profile extends React.Component {

  constructor(props) {

    super(props);

    this.state = {
      activityToEdit: {},
      courses: '',
      distance: 0,
      editing: false,
      education: '',
      error: {},
      image: '',
      schedule: '', // ProfileSchedule[0],
      selectedFile: {},
      showActivityModal: false,
      tagName: '',
      tags: [],
      currentUser: {}
    }
  }

  profileLevel = '';
  completeness = 0;

  componentDidMount() {

    const { me } = this.props;

    let setStateCb = () => {

      this.getProfileLevel();
    }

    if (!me.loggedIn) {
      this.props.history.push('/login');

      return
    }

    this.props.getUserActivities();

    this.setState({
      currentUser: me.user,
      image: me.user.image,
      tags: me.user.tags,
      education: me.user.education.join(','),
      courses: me.user.courses.join(',')
    }, setStateCb)
  }

  // On any update we update the profile level
  componentDidUpdate() {

    this.getProfileLevel();
  }


  // Add the selected skill to the user skills' array
  onSelectSkill = tagName => {

    const { tags } = this.state;

    if (tags.indexOf(tagName) < 0) {

      this.setState({
        tags: [...tags, tagName]
      });

      this.updateSkillsError(tags);
    }

  }

  // Remove skill tag
  onRemoveSkill = tagName => {

    let { tags } = this.state;

    tags = tags.filter( tag => (tag !== tagName));

    this.setState({ tags });

    this.updateSkillsError(tags);
  }

  // Check skills error
  updateSkillsError = tags => {

    let { error } = this.state;

    if (tags.length >= 3) {
      delete error.tags

    } else {
      error.tags = 'Choose at least 3 skills for your profile'
    }

    this.setState({ error });
  }


  handleDistanceChange = value => {

    this.setState({
      distance: value
    })
  }

  handleInputChange = name => event => {

    this.setState({
      [name]: event.target.value
    })
  }

  handleRadioChange = () => event => {

    this.setState({
      schedule: event.target.value
    })
  }


  openCloseActivityModal = value => () => {

    this.setState({
      showActivityModal: value
    });

    if (!value) {

      this.setState({
        activityToEdit: {}
      });
    }
  }

  addActivity = (act, isEdit) => {

    const { me } = this.props;

    const newActivity = {
      key: me.activities[me.activities.length - 1].key + 1,
      name: act.name,
      type: act.type,
      target: act.target,
      price: act.price,
      equipment: act.equipment,
      user_id: me.user.id
    }

    const cb = () => {

      this.props.getUserActivities();
    }

    if (isEdit) {
      this.props.editActivity(newActivity, cb)

    } else {

      this.props.createActivity(newActivity, cb);
    }

    this.setState({
      showActivityModal: false
    })
  }

  activityClick = act => () => {

    this.setState({
      activityToEdit: act,
      showActivityModal: true
    })
  }


  // Click on edit profile info
  toggleEditMode = () => () => {

    let { editing } = this.state;

    this.setState({
      editing: !editing
    })
  }

  saveProfileInfoChanges = (name, email) => {

    const { me } = this.props;

    let newUserInfo = {
      ...me.user,
      name,
      email
    }

    this.props.updateProfile(newUserInfo);

    this.setState({
      editing: false
    })
  }


  saveAllChanges = () => {

    const { me } = this.props;

    const { tags, distance, schedule, education, courses } = this.state;

    let newUserInfo = {
        ...me,
        distance,
        schedule,
        education: education.split(','),
        courses: courses.split(',')
      }

    // Post user information
    this.props.updateProfile(newUserInfo);

    // Post user skills in tags array
    tags.forEach(skill => {
      this.props.putSkill(skill);
    })

    NotificationManager.success('Profile has been updated.', 'Success');
  }


  scoreToLevel = score => {

    if (score < 20)
      return 'Noob';

    if (score < 40)
      return 'Beginner'

    if (score < 60)
      return 'Amateur';

    if (score < 80)
      return 'Advanced';

    return 'Expert';
  }

  getProfileLevel = () => {

    const { tags, distance, education, courses, schedule } = this.state;

    let score = 0;

    // 3 - 5 points, one per skill
    score += tags.length;

    // 0.5 per skill after the 5th
    if (tags.length > 5)
      score += 0.5 * (tags.length - 5);

    if (distance > 0)
      score++;

    if (education.length > 0)
      score++;

    if (courses.length > 0)
      score++;

    if (schedule.length > 0)
      score++;

    // 3 points max
    if (Activities__exampleData.length > 0)
      score++;

    if (Activities__exampleData.length > 2)
      score += 2;


    score = Math.min(score * 10, 100);

    this.profileLevel = this.scoreToLevel(score);
    this.completeness = score;
  }


  postUserImage = () => {

    const { selectedFile } = this.state;

    this.props.uploadImage(selectedFile, (res, msg = '') => {

      if (res === 'error') {
        NotificationManager.error(msg, 'Image Upload Failed', 3000, ()=> {});
      }
    })

  }

  onChangePhoto = () => event => {

    const { files } = event.target,
      ValidTypes = ['image/jpeg', 'image/png', 'image/jpg', 'image/JPG', 'image/PNG', 'image/JPEG'];

    if (!files.length) {

      return;
    }

    if (ValidTypes.indexOf(files[0].type) === -1) {

      NotificationManager.error('Invalid File Type', 'Error!', 5000, () => { });

      return;
    }

    let reader = new FileReader();

    reader.onload = () => {

      this.setState({
        image: reader.result,
        selectedFile: files[0]
      });

      this.postUserImage();
    };

    reader.readAsDataURL(files[0]);
  }


  render() {

    const { classes, screenWidth, getSkills, me } = this.props;

    const { activityToEdit, courses, distance, editing, education, error, image,
        tagName, tags, schedule, showActivityModal } = this.state;

    return (

      <div>

        <AvatarSection>
          <Grid container justify="center">
            <Grid item xs={12} md={2}>
              <Grid container justify="center">
                <Avatar src={image} className={classes.avatar} />
                <label>
                  <input type="file" ref={ref => this.fileEvent = ref} className={classes.fileInput} onChange={this.onChangePhoto()}/>
                  <WhiteSText justify="center">Change Photo</WhiteSText>
                </label>
              </Grid>
            </Grid>

            <Grid item xs={10} md={4}>
              <Bold><WhiteLText justify={screenWidth < 768 ? 'center' : 'left'}>This is your profile, {me.user.name.split(' ')[0]}</WhiteLText></Bold>
              <WhiteMText justify={screenWidth < 768 ? 'center' : 'left'}>{me.user.name} - <span>{me.user.email}</span></WhiteMText>
              <RightView>
                <Fab color="default" aria-label="Edit" className={classes.editIcon} onClick={this.toggleEditMode()}>
                  <Icon>edit_icon</Icon>
                </Fab>
              </RightView>
            </Grid>
          </Grid>
        </AvatarSection>

        <div style={{padding: '50px 0'}}>
          <Grid container justify="center">
            <Grid item xs={12} md={6}>
              <MText justify="center">Profile Level: <span style={{color: Colors.green}}>{this.profileLevel}</span></MText>
              <GraySText justify="center">Add more skills and activities to your profile to get upgraded.</GraySText>
              <ProgressBar value={this.completeness} height={15} showValue={false} width={screenWidth * 0.5}/>
            </Grid>
          </Grid>
        </div>

        <Grid container justify="center">
          <Grid item xs={10} md={8} lg={6}>
            <MText>Habilidades</MText>
            <AutoSuggestInput
              value={tagName}
              label="Habilidades"
              placeholder="Type here to add some skills to your profile"
              onChange={text => this.setState({tagName: text})}
              onSelect={tagName => this.onSelectSkill(tagName)}
              selectedValues={tags}
              getSkills={getSkills}
            />
          </Grid>
        </Grid>

        <Grid container justify="center">
          <Grid item xs={10} md={8} lg={6}>

            { error.tags && tags.length < 3 &&
              <ErrorText>{error.tags}</ErrorText>
            }

            <WrapView>
              {
                tags.map(tag => (
                  <Chip
                    key={tag}
                    label={tag}
                    color="default"
                    onDelete={() => this.onRemoveSkill(tag)}
                    variant="default"
                    className={classes.tagChip}
                  />
                ))
              }
            </WrapView>
          </Grid>
        </Grid>

        <Grid container justify="center">
          <Grid item xs={10} md={8} lg={6}>
            <CustomSlider
              label="Distancia máxima de desplazamiento"
              value={distance}
              maxValue={50}
              onChange={this.handleDistanceChange}
            />
          </Grid>
        </Grid>

        <Grid container justify="center">
          <Grid item xs={10} md={8} lg={6}>
            <MText>Calificaciones</MText>
            <Grid container justify="space-between">
              <Grid item xs={12} md={5}>
                <TextField
                  label="GRADO"
                  placeholder="GRADO"
                  className={classes.textField}
                  value={education}
                  onChange={this.handleInputChange('education')}
                  margin="normal"
                  variant="outlined"
                />
              </Grid>

              <Grid item xs={12} md={5}>
                <TextField
                  label="CURSOS"
                  placeholder="CURSOS"
                  className={classes.textField}
                  value={courses}
                  onChange={this.handleInputChange('courses')}
                  margin="normal"
                  variant="outlined"
                />
              </Grid>
            </Grid>
          </Grid>
        </Grid>

        <Grid container justify="center">
          <Grid item xs={10} md={8} lg={6}>
            <MText>Horario disponible</MText>
            <RadioGroup
              className={classes.radioGroup}
              value={schedule}
              onChange={this.handleRadioChange()}
            >
              {
                ProfileSchedule.map(schedule => (
                  <FormControlLabel
                    key={schedule}
                    classes={{label: classes.label}}
                    value={schedule}
                    control={<Radio classes={{root: classes.rootRadio}} checkedIcon={<div className={classes.radio}/>} />}
                    label={schedule}
                  />
                ))
              }
            </RadioGroup>
            <Divider />
          </Grid>
        </Grid>

        <Grid container justify="center">
          <Grid item xs={10} md={8} lg={6}>
            <MText>Actividades</MText>
            <Grid container>
              {
                me.activities.map(activity => (
                  <Grid item xs={6} md={4} key={activity.key}>
                    <Button onClick={this.activityClick(activity)} className={classes.ActivityCardButton}>
                      <ActivityCard>
                        <Grid container>
                          <GraySText style={{height: 36, overflow: 'hidden', lineHeight: '18px', width: '100%'}}>{activity.name}</GraySText>
                          <LightGraySText style={{margin: 0}}>{activity.target}</LightGraySText>
                        </Grid>
                        <div className={classes.activityPriceView}>
                          <GraySText>{activity.type}</GraySText>
                          <LightGraySText>• {activity.price}€ / sesión</LightGraySText>
                        </div>
                      </ActivityCard>
                    </Button>
                  </Grid>
                ))
              }

              <Grid item xs={6} md={4} key="new-activity">
                <Button onClick={this.openCloseActivityModal(true)} className={classes.ActivityCardButton}>
                  <AddActivityCard>
                    <AddIcon className={classes.addActivityIcon} />
                    <GraySText justify="center">AÑADIR ACTIVIDAD</GraySText>
                  </AddActivityCard>
                </Button>
              </Grid>
            </Grid>
            <Divider style={{marginTop: 50}}/>
          </Grid>
        </Grid>


        <Grid container justify="center">
          <Grid item xs={10} md={8} lg={6}>
            <RightView style={{padding: '40px 0 100px 0'}}>
              <OrangeButton text="Guardar cambios" paddingHorizontal={40} onPress={() => this.saveAllChanges()}/>
            </RightView>
          </Grid>
        </Grid>

        <EditDialog
          open={editing}
          onSubmit={this.saveProfileInfoChanges}
          onClose={this.toggleEditMode}
          name={me.user.name}
          email={me.user.email}
        />

        <NewActivity
          activity={activityToEdit}
          visible={showActivityModal}
          onCancel={this.openCloseActivityModal(false)}
          onSubmit={(activityInfo, isEdit) => this.addActivity(activityInfo, isEdit)}
        />

      </div>
    )
  }
}


Profile.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  screenWidth: state.screenReducer.width,
  me: state.userReducer,
})

const mapDispatchToProps = ({
  updateProfile: authActions.updateProfile,
  uploadImage: authActions.uploadImage,
  getSkills: authActions.getSkills,
  putSkill: authActions.putSkill,
  getUserActivities: activityActions.getUserActivities,
  createActivity: activityActions.createActivity,
})

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(Profile));
