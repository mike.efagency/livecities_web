import * as React from 'react';
import { withStyles } from '@material-ui/core/styles';

import PropTypes from 'prop-types';

import { Button, Dialog, DialogTitle, Grid, TextField } from '@material-ui/core';

import { RightView } from '../../styles/global';

const styles = {
  textField: {
    width: '100%'
  },
  button: {
    marginLeft: 20,
    marginBottom: 20,
    marginTop: 20
  }
}


class EditDialog extends React.Component {

  constructor(props) {

    super(props);

    this.state = {
      name: props.name,
      email: props.email
    }
  }

  handleInputChange = (name) => event => {

    this.setState({[name]: event.target.value})
  };

  handleClose = () => () => {
    this.props.onClose();
  };


  render() {

    const { classes, ...other } = this.props;

    const { name, email } = this.state;

    return (

      <Dialog onClose={this.handleClose()} aria-labelledby="simple-dialog-title" {...other}>
        <DialogTitle id="simple-dialog-title">Change your name or email address</DialogTitle>

        <Grid container justify="center">
          <Grid item xs={10}>
            <TextField
              label="Name"
              className={classes.textField}
              value={name}
              onChange={this.handleInputChange('name')}
              margin="normal"
              variant="outlined"
            />
            <TextField
              label="Email"
              className={classes.textField}
              value={email}
              onChange={this.handleInputChange('email')}
              margin="normal"
              variant="outlined"
            />
            <RightView>
              <Button variant="contained" className={classes.button} onClick={this.props.onClose()}>
                Cancel
              </Button>
              <Button variant="contained" color="primary" className={classes.button} onClick={() => this.props.onSubmit(name, email)}>
                Save
              </Button>
            </RightView>
          </Grid>
        </Grid>
      </Dialog>
    );
  }
}

EditDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  email: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired
};

export default withStyles(styles)(EditDialog);
