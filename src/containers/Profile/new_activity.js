import * as React from 'react';
import { withStyles } from '@material-ui/core/styles';

import PropTypes from 'prop-types';

import { Grid, Divider, RadioGroup, FormControlLabel, Radio, Button } from '@material-ui/core';
import Icon from '@material-ui/core/Icon';

import { IfView, CustomInput, OrangeButton } from '../../components';
import { ActivityTypes, ActivityTargets, ActivityEquipments } from '../../lib/constants';
import { validatePrice } from '../../lib/operators';
import { Colors } from '../../lib/theme';

import { ErrorText, WhiteMText, WhiteSText, Bold, RightView } from '../../styles/global';
import { NewActivityWrapper } from '../../styles/profile';

const styles = () => ({
  divider: {
    backgroundColor: 'rgb(120, 20, 220)',
  },
  closeButton: {
    minWidth: 40,
    height: 40,
    borderRadius: 40,
    padding: 0,
    backgroundColor: 'rgba(200, 200, 200, 0.6)'
  },
  closeIcon: {
    fontSize: 30,
    color: 'white'
  },
  radioGroup: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 30,
    paddingRight: 30,
    paddingBottom: 30
  },
  radio: {
    width: 8,
    height: 8,
    borderRadius: 24,
    border: '8px solid ' + Colors.green,
    backgroundColor: 'white'
  },
  rootRadio: {
    width: 24,
    height: 24,
    color: 'white'
  },
  label: {
    color: 'white'
  }
});

const initialState = {
  key: null,
  name: '',
  type: ActivityTypes[0],
  target: ActivityTargets[0],
  price: '0',
  materialsIncluded: ActivityEquipments[0],
  error: {},
  loading: false
}

class NewActivity extends React.Component {

  constructor(props) {

    super(props);

    this.state = initialState;
  }

  isEdit = false;

  componentDidMount() {

    this.setExistingActivity();
  }

  componentDidUpdate(prevProps) {

    if (this.props.visible && !prevProps.visible) {
      // The update was to show the modal
      this.setExistingActivity();
    }
  }

  setExistingActivity = () => {

    const { activity } = this.props,
      initialState = this.state;

    if (Object.keys(activity).length === 0) {

      this.isEdit = false;

      this.resetForm();

      return;
    }

    this.isEdit = true;

    this.setState({
      ...initialState, ...activity
    })
  }

  resetForm = () => {

    this.setState(
      initialState
    )
  }


  handleInputChange = name => event => {

    this.setState({
      [name]: event.target.value,

    }, this.checkFormErrors);
  };

  checkFormErrors = () => {

    const { name, price, error } = this.state;

    if (name.length === 0) {
      error.name = 'This field is required'
    } else {
      delete error.name
    }

    if (price.length === 0) {
      error.price = 'This field is required'
    } else {

      delete error.price

      if (!validatePrice(price)) {
        error.price = 'Wrong format'

      } else {
        delete error.price
      }
    }

    this.setState({ error });

    return Object.keys(error).length === 0
  }


  addActivity = () => {

    const { key, name, type, target, price, materialsIncluded } = this.state;

    if (!this.checkFormErrors()) {

      return;
    }

    const activity = {
      key,
      name,
      type,
      target,
      price,
      materialsIncluded
    }

    this.props.onSubmit(activity, this.isEdit);
  }


  render() {

    const { classes, visible } = this.props;

    const { error, name, type, target, price, materialsIncluded, loading } = this.state;

    return (

      <IfView condition={visible}>
        <NewActivityWrapper>
          <Grid container justify="center">

            <Grid item xs={10} md={7}>
              <RightView>
                <Button onClick={() => this.props.onCancel()} className={classes.closeButton}>
                  <Icon className={classes.closeIcon}>close</Icon>
                </Button>
              </RightView>
              <Bold><WhiteMText justify="center">{`${this.isEdit ? 'EDITAR' : 'NUEVA'} ACTIVIDAD`}</WhiteMText></Bold>
              <WhiteSText justify="center">Suspendisse luctus ultrices lectus vel porttitor. In lectus diam, hendrerit a auctor ac, hendrerit a massa.</WhiteSText>
              <Divider className={classes.divider} style={{marginBottom: 50}}/>

              <CustomInput
                label="Título"
                value={name}
                error={error.name ? true : false}
                onChange={this.handleInputChange('name')}
                onBlur={() => this.checkFormErrors()}
              />

              { error.name &&
                <ErrorText>{error.name}</ErrorText>
              }

              <CustomInput
                type="select"
                label="Formato"
                value={type}
                onChange={this.handleInputChange('type')}
                options={ActivityTypes}
              />

              <WhiteMText>&nbsp;&nbsp;Público objetivo</WhiteMText>
              <RadioGroup
                className={classes.radioGroup}
                value={target}
                onChange={this.handleInputChange('target')}
              >
                { ActivityTargets.map(target => (
                  <FormControlLabel
                    key={target}
                    classes={{label: classes.label}}
                    value={target}
                    control={<Radio classes={{root: classes.rootRadio}} checkedIcon={<div className={classes.radio}/>} />}
                    label={target}
                  />
                )) }
              </RadioGroup>

              <Grid container justify="space-between">

                <Grid item xs={12} md={5}>
                  <CustomInput
                    label="Precio (€)"
                    value={price}
                    error={error.price ? true : false}
                    onChange={this.handleInputChange('price')}
                    onBlur={() => this.checkFormErrors()}
                  />

                  { error.price &&
                    <ErrorText>{error.price}</ErrorText>
                  }
                </Grid>

                <Grid item xs={12} md={6}>
                  <CustomInput
                    type="select"
                    label="Incluye el material?"
                    value={materialsIncluded}
                    onChange={this.handleInputChange('materialsIncluded')}
                    options={ActivityEquipments}
                  />
                </Grid>
              </Grid>
              <Divider className={classes.divider} style={{marginTop: 50}}/>

              <RightView paddingTop={30}>
                <OrangeButton
                  text={`${this.isEdit ? 'GUARDAR' : 'CREAR'} ACTIVIDAD`}
                  paddingHorizontal={40}
                  loading={loading}
                  onPress={this.addActivity}
                />
              </RightView>
            </Grid>
          </Grid>
        </NewActivityWrapper>

        <div />
      </IfView>
    )

  }
}

NewActivity.propTypes = {
  activity: PropTypes.object,
  visible: PropTypes.bool.isRequired,
  onCancel: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired
};

export default withStyles(styles)(NewActivity);
