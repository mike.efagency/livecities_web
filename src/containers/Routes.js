import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Home from './Home';
import Login from './Auth/login';
import SignUp from './Auth/signup';
import Profile from './Profile';
import NewProject from './Project/create';
import NewResource from './Space/create';
import { Header, Footer } from '../components';
import InvalidPage from './Invalid';
import Search from './Search';
import Space from './Space';

const Container = ({children, ...props}) => (
  <div className="app">
    <Header {...props} background="white"/>
    <div style={{paddingTop: 80}}>
      {children}
    </div>
    <Footer {...props}/>
  </div>
);

const Router = props => (
  <BrowserRouter>
    <Container {...props}>
      <Switch>
        <Route exact path="/" component={Login}/>
        <Route exact path="/login" component={Login}/>
        <Route exact path="/signup" component={SignUp}/>
        <Route exact path="/myprofile" component={Profile} />
        <Route exact path="/home" component={Home}/>
        <Route exact path="/project/new" component={NewProject} />
        <Route exact path="/resource/new" component={NewResource} />
        <Route exact path="/search/:type" component={Search} />
        <Route exact path="/search" component={Search} />
        <Route exact path="/resource/:id" component={Space} />
        <Route exact path="*" component={InvalidPage} />
      </Switch>
    </Container>
  </BrowserRouter>
);

export default Router;
