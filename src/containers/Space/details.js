import * as React from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';

import Popover, { ArrowContainer } from 'react-tiny-popover';
import StarRatingComponent from 'react-star-rating-component';

import { Grid, Divider, Icon, TextField } from '@material-ui/core';

import { OrangeButton, CustomDatePicker } from '../../components';
import { MyGoogleMap } from '../../components/MyGoogleMap';
import { spaceActions } from '../../redux/actions';
import { defaultAvatar, surfaceIcon, capacityIcon, costIcon } from '../../lib/images';
import { Colors } from '../../lib/theme';

import 'rc-calendar/assets/index.css';

import { GrayMText, MText, SText, SBView, LightGrayMText, Row, GraySText, WhiteMText, WhiteSText } from '../../styles/global';
import { EquipmentView, EquipmentTag, TableView, TableIcon, Avatar, ApplyContent } from '../../styles/space';

const { Circle } = require('react-google-maps/lib/components/Circle');

const styles = {
  titleIcon: {
    color: Colors.green
  },
  popoverDivider: {
    color: '#731DC4'
  },
  textArea: {
    width: '100%',
    minHeight: 100,
    background: 'white',
    borderRadius: 4
  },
  button: {
    marginLeft: 20,
    marginBottom: 20,
    marginTop: 20
  }
}


class SpaceDetailView extends React.Component {

  constructor(props) {

    super(props);

    this.state = {
      calendarFocused: true,
      dates: ['', ''],
      isPopoverOpen: false,
      selectedDate: new Date(),
      message: ''
    }
  }


  handleInputChange = name => event => {

    this.setState({
      [name]: event.target.value
    });
  }

  handleDateChange = date => {

    console.log(date)
  }

  randomRatingTop5 = () => {

    return Math.floor((Math.random() * 5) + 1);
  }

  renderSendMessagePopover = () => {

    const { selectedDate, message, isPopoverOpen } = this.state;
    const { classes, selectedResource, screenWidth } = this.props;

    const width = screenWidth < 400 ? screenWidth - 80 : 400;

    return(
      <ApplyContent width={width}>
        <Grid container>
          <Grid item xs={12}>
            <WhiteMText justify="center">ENVIAR SOLICITUD</WhiteMText>

            <SBView>
              <WhiteSText>{selectedResource.price} per use</WhiteSText>
              <Row>
              <WhiteSText>{23}</WhiteSText>
                <StarRatingComponent
                  name={'pop-rating'}
                  starCount={5}
                  value={selectedResource.average_rating || this.randomRatingTop5()}
                  editing={true}
                  emptyStarColor={Colors.lightgray}
                />
              </Row>
            </SBView>

            <Divider className={classes.popoverDivider}/>
            <WhiteSText>Fechas</WhiteSText>
            <CustomDatePicker
              date={selectedDate}
              onSelect={date => this.handleDateChange(date)}
              borderColor="white"
              dateColor="white"
            />

            <WhiteSText>Message</WhiteSText>
            <TextField
              label=""
              className={classes.textArea}
              value={message}
              onChange={this.handleInputChange('message')}
              margin="normal"
              variant="outlined"
              multiline
              rows={5}
            />
            <br />
            <Divider className={classes.popoverDivider}/>
            <br />
            <Grid container justify="flex-end">
              <div onClick={() => this.setState({ isPopoverOpen: !isPopoverOpen })}>
                Cancel
              </div>
              <OrangeButton text="Send" onPress={this.onSendMessage} />
            </Grid>
            <br />
            <br />
          </Grid>
        </Grid>
      </ApplyContent>
    )
  }

  onSendMessage = () => {

    let { isPopoverOpen } = this.state;

    const { user, selectedResource } = this.props;

    this.setState({
      isPopoverOpen: !isPopoverOpen
    });

    this.props.sendMessageToOwner({
      user_id: user.id,
      resource_id: selectedResource.id,
      dates: [this.state.selectedDate.toISOString],
      message: this.state.message
    });
  }


  render() {

    const { isPopoverOpen } = this.state;
    const { selectedResource, classes } = this.props;

    const resource = selectedResource || {};

    console.log(resource);

    return (
      <Grid container>
        <Grid container justify="space-between">

          <Grid item xs={12} md={5}>
            <GraySText>SPACE</GraySText>
            <MText>{resource.name}</MText>
            <Divider />
            <SText>{resource.description}</SText>
            <br />
            <MText>Equipment</MText>
            <Divider />
            <EquipmentView>
              {
                resource.equipment !== undefined &&
                resource.equipment.map((equipment) => {
                  return(
                    <EquipmentTag key={equipment}>{equipment}</EquipmentTag>
                  )
                })
              }
            </EquipmentView>
            <br />
            <TableView>
              <SBView padding="0 20px">
                <Row>
                  <span><TableIcon src={surfaceIcon} /></span>
                  <MText>Surface</MText>
                </Row>
                <LightGrayMText>{resource.squaredfeet} m2</LightGrayMText>
              </SBView>
              <Divider />

              <SBView padding="0 20px">
                <Row>
                  <span><TableIcon src={capacityIcon} /></span>
                  <MText>Capacity</MText>
                </Row>
                <LightGrayMText>{resource.capacity}</LightGrayMText>
              </SBView>
              <Divider />

              <SBView padding="0 20px">
                <Row>
                  <span><TableIcon src={costIcon} /></span>
                  <MText>Cost</MText>
                </Row>
                <LightGrayMText>{resource.price}€ / use - 24€ / h</LightGrayMText>
              </SBView>
            </TableView>
          </Grid>

          <Grid item xs={12} md={6}>
            <SBView style={{height: 108}}>
              <Row>
                <Avatar src={resource.owner ? resource.owner.image || defaultAvatar : defaultAvatar} />
                <GrayMText>{resource.owner ? resource.owner.name : ''}</GrayMText>
              </Row>
              <Popover
                  isOpen={isPopoverOpen}
                  position="bottom"
                  align="end"
                  disableReposition
                  content={({position, targetRect, popoverRect}) => (
                    <ArrowContainer
                      position={position}
                      targetRect={targetRect}
                      popoverRect={popoverRect}
                      arrowColor={'#3D2552'}
                      arrowSize={10}
                      arrowStyle={{ opacity: 0.9 }}
                    >
                      {this.renderSendMessagePopover()}
                    </ArrowContainer>
                  )}>
                  <div onClick={() => this.setState({ isPopoverOpen: !isPopoverOpen })}>
                    Click me!
                  </div>
              </Popover>
            </SBView>

            <SBView>
              <MText>Location</MText>
              <Icon className={classes.titleIcon}>location_on</Icon>
            </SBView>
            <Divider />

            <br />

            <div style={{height: 300}}>
              <MyGoogleMap center={{ lat: resource.latitude, lng: resource.longitude }} zoom={14}>
                <Circle
                  center={{ lat: resource.latitude, lng: resource.longitude }}
                  radius={300}
                  options={{
                    strokeColor: Colors.green,
                    strokeOpacity: 0.8,
                    strokeWeight: 2,
                    fillColor: Colors.green,
                    fillOpacity: 0.35,
                  }}
                />
              </MyGoogleMap>
            </div>
          </Grid>
        </Grid>

      </Grid>
    );
  }
}

const mapStateToProps = (state) => ({
  screenWidth: state.screenReducer.width,
  user: state.userReducer.user,
  selectedResource: state.spaceReducer.selectedResource
});

const mapDispatchToProps = ({
  sendMessageToOwner: spaceActions.sendMessageToOwner
})

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(SpaceDetailView));
