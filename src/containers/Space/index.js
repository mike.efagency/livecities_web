import * as React from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';

import { Grid, Fab, Icon, Link, Divider } from '@material-ui/core';

import { IfView } from '../../components';
import { spaceActions } from '../../redux/actions';

import SpaceDetailView from './details';
import ResourceCard from '../Home/ResourceCard';

import { WhiteLText, SBView, MText, LightGrayMText } from '../../styles/global';
import { ImageView } from '../../styles/space';

const styles = {
  shareButton: {
    backgroundColor: 'transparent',
    border: '1px solid white',
    padding: '0 30px',
    width: 'auto',
    height: 40,
    borderRadius: 20,
    marginRight: 30
  },
  viewMoreButton: {
    backgroundColor: 'transparent',
    border: '1px solid white',
    color: 'white',
    height: 40,
    borderRadius: 20,
    padding: '0 20px'
  },
  buttonIcon: {
    marginRight: 10
  },
  shareButtonView: {
    display: 'flex',
    alignItems: 'center'
  }
}


class Space extends React.Component {

  componentDidMount() {

    const { id } = this.props.match.params

    this.props.getSpace(id);
  }


  toResourcePage = id => {

    this.props.history.push(`/resource/${id}`);
  }


  render() {

    const { screenWidth, classes, homeResources, selectedResource } = this.props;

    const resource = selectedResource || {};

    console.log(resource);

    return (

      <Grid container>

        <ImageView>
          <IfView condition={screenWidth < 640}>
            <Grid container>
              <Grid item xs={12}>
                <WhiteLText>{resource.name}</WhiteLText>
              </Grid>
              <Grid item xs={12}>
                <Grid container justify="space-between">
                  <Fab
                    variant="round"
                    size="small"
                    color="primary"
                    aria-label="Add"
                    className={classes.shareButton}
                  >
                    <Icon className={classes.buttonIcon}>share</Icon>
                    Share
                  </Fab>
                  <Fab
                    variant="round"
                    size="small"
                    color="primary"
                    aria-label="Add"
                    className={classes.shareButton}
                  >
                    <Icon className={classes.buttonIcon}>bookmark</Icon>
                    Save
                  </Fab>
                </Grid>
              </Grid>
            </Grid>
            <Grid container>
              <Grid item xs={8}>
                <WhiteLText>{resource.name}</WhiteLText>
              </Grid>
              <Grid item xs={4} className={classes.shareButtonView}>
                <Grid container justify="flex-end">
                  <Fab
                    variant="round"
                    size="small"
                    color="primary"
                    aria-label="Add"
                    className={classes.shareButton}
                  >
                    <Icon className={classes.buttonIcon}>share</Icon>
                    Share
                  </Fab>
                  <Fab
                    variant="round"
                    size="small"
                    color="primary"
                    aria-label="Add"
                    className={classes.shareButton}
                  >
                    <Icon className={classes.buttonIcon}>bookmark</Icon>
                    Save
                  </Fab>
                </Grid>
              </Grid>
            </Grid>
          </IfView>
        </ImageView>

        <Grid container justify="center">
          <Grid item xs={11} md={9}>

            <SpaceDetailView />
            <Divider />

            <SBView>
              <MText>Recursos más solicitados</MText>
              <Link href="/search/resource">{"Ver todos >"}</Link>
            </SBView>

            <IfView condition={homeResources.length === 0}>
              <LightGrayMText justify="center">No results</LightGrayMText>
              <Grid container>
              {
                homeResources.slice(0, 4).map(resourceItem => (
                  <Grid key={resourceItem.id} item xs={12} md={6} lg={3}>
                    <ResourceCard key={resourceItem.id} resource={resourceItem} onClick={() => this.toResourcePage(resourceItem.id)}/>
                  </Grid>
                ))
              }
              </Grid>
            </IfView>

          </Grid>
        </Grid>

      </Grid>
    );
  }
}

const mapStateToProps = (state) => ({
  screenWidth: state.screenReducer.width,
  homeResources: state.homeReducer.resources,
  selectedResource: state.spaceReducer.selectedResource
});

const mapDispatchToProps = ({
  getSpace: spaceActions.getSpace
})

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(Space));
