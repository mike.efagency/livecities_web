import * as React from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';

import { NotificationManager } from 'react-notifications';
import CircularProgressbar from 'react-circular-progressbar';

import { Button, Chip, Grid, Divider, TextField } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';

import { AutoCompleteInput, AddressPicker, IfView, OrangeButton } from '../../components';
import { spaceActions } from '../../redux/actions';
import { Space__equipmentList } from '../../lib/constants';
import { Colors } from '../../lib/theme';

import { SBView, LText, GraySText, MText, ErrorText, RightView, WrapView } from '../../styles/global';

const styles = theme => ({
  button: {
    width: '100%',
    padding: 10
  },
  textField: {
    width: '100%'
  },
  textArea: {
    width: '100%',
    minHeight: 100
  },
  progressLeft: {
    [theme.breakpoints.down('sm')]: {
      flex: 2,
      marginRight: 10
    },
    [theme.breakpoints.up('sm')]: {
      flex: 4,
      marginRight: 80
    },
  },
  circle: {
    flex: 1,
    marginLeft: 60
  },
  imgUploadLabel: {
    width: '100%',
    height: '100%',
    display: 'block',
    cursor: 'pointer'
  },
  avatarView: {
    cursor: 'pointer',
    marginRight: 20,
    marginTop: 20,
    marginBottom: 20,
    width: 175,
    height: 175,
    borderRadius: 175,
    border: '1px solid lightgray',
    padding: 0
  },
  avatar: {
    width: 175,
    height: 175,
    borderRadius: 175,
    objectFit: 'cover',
    position: 'absolute',
    top: 0,
    left: 0,
    cursor: 'pointer'
  },
  avatarAddIcon: {
    color: 'green'
  },
  fileInput: {
    width: 0,
    height: 0
  },
  datePicker: {
    width: '100%',
    border: '1px solid #ced4da',
    borderRadius: 4,
    height: 40,
    overflow: 'hidden',
    margin: 0
  },
  datePickerInput: {
    minHeight: 60,
    marginTop: -10,
    paddingLeft: 12,
    color: Colors.gray
  },
  activityPriceView: {
    [theme.breakpoints.up('sm')]: {
      display: 'flex',
      justifyContent: 'space-between'
    },
  },
  addActivityIcon: {
    fontSize: 50,
    color: Colors.green
  },
  ActivityCardButton: {
    width: '100%'
  },
  tag: {
    margin: 5
  },
});


class NewResource extends React.Component {

  constructor(props) {

    super(props);

    this.state = {
      name: '',
      description: '',
      address: '',
      latitude: '',
      longitude: '',
      capacity: '',
      surface: '',
      price: '',
      equipmentName: '',
      equipment: [],
      images: [],
      error: {},
      loading: false
    }
  }

  completeness = 0;

  componentDidMount() {

    const { me } = this.props;

    if (!me.loggedIn) {
      this.props.history.push('/login');

      return
    }

    // Init this array to show the 3 upload image buttons
    let images = [1,2,3].map(() => {
      return ({
        imageURL: '',
        selectedFile: {}
      });
    });

    this.setState({
      images
    })
  }

  // On any state update we update the resource level
  componentDidUpdate() {

    this.updateResourceCompletenessLevel();
  }


  handleInputChange = name => event => {

    this.setState({
      [name]: event.target.value
    })
  }

  handleAddressChange = address => {

    this.setState({
      address
    })
  }

  handleLocationChange = latLng => {

    console.log(latLng);

    this.setState({
      latitude: latLng.lat,
      longitude: latLng.lng
    })
  }


  // Add the selected equipment to the resource's array
  onSelectEquipment = item => {

    const { equipment } = this.state;

    if (equipment.indexOf(item) < 0) {

      this.setState({
        equipment: [...equipment, item]
      });
    }

  }


  // Remove equipment tag
  onRemoveEquipment = itemToRemove => {

    let { equipment } = this.state;

    equipment = equipment.filter( item => (item !== itemToRemove));

    this.setState({ equipment });
  }


  checkFormErrors = () => {

    const { name, description, address, capacity, surface,
      price, equipment, images, error } = this.state;

    const fields = [{name}, {description}, {address},
      {capacity}, {surface}, {price}, {equipment}];

    fields.forEach(field => {
      let key = Object.keys(field)[0],
        value = field[key];

      if (value.length === 0) {
        error[key] = 'This field is required'
      } else {
        delete error[key]
      }
    })

    let validImagesCount = 0;

    images.forEach(img => {
      if (img.imageURL.length) {
        validImagesCount++
      }
    })

    if (validImagesCount >= 3) {
      delete error.images;
    } else {
      error.images = `Upload ${3 - validImagesCount} images to publish your resource`;
    }

    this.setState({ error });

    return Object.keys(error).length === 0
  }


  updateResourceCompletenessLevel = () => {

    const { name, description, address, capacity, surface,
      price, equipment, images } = this.state;

    let score = 0;

    // Images: 1 point for each img: 3
    // Equipment: 0.5 points for each tag

    if (name.length)
      score += 10;

    if (description.length)
      score += 10;

    if (address.length)
      score += 10;

    if (capacity.length)
      score += 10;

    if (surface.length)
      score += 10;

    if (price.length)
      score += 10;

    images.forEach(img => {
      if (img.imageURL.length) {
        score += 10
      }
    })

    score += 5 * equipment.length;

    score = Math.min(score, 100);

    this.completeness = score;
  }


  onChangePhoto = (index) => event => {

    const { files } = event.target,
      ValidTypes = ['image/jpeg', 'image/png', 'image/jpg', 'image/JPG', 'image/PNG', 'image/JPEG'];

    if (!files.length) {

      return;
    }

    if (ValidTypes.indexOf(files[0].type) === -1) {

      NotificationManager.error('Invalid File Type', 'Error!', 5000, () => { });

      return;
    }

    let reader = new FileReader();

    reader.onload = () => {

      const { images } = this.state,
        newImage = {
          imageURL: reader.result,
          selectedFile: files[0]
        };

      images[index] = newImage;

      this.setState({
        images,
      });
    };

    reader.readAsDataURL(files[0]);
  }


  onSaveChanges = () => {

    const { name, description, latitude, longitude, capacity, surface,
      price, equipment, images } = this.state;

    if (!this.checkFormErrors()) {

      return;
    }

    // Post new project
    this.setState({
      loading: true
    })

    const newSpace = {
      name,
      description,
      latitude,
      longitude,
      capacity,
      surface,
      price,
      equipment
    }

    this.props.createSpace(newSpace, images, res => {

      this.setState({
        loading: false
      });

      if (res === 'error') {
        NotificationManager.error('Resource upload failed', 'Try again', 3000, ()=> {});

      } else {

        NotificationManager.success('Resource created!', 'Success', 3000, ()=> {});

        this.props.history.push('/home');
      }
    })
  }


  render() {

    const { classes } = this.props;

    const { name, description, capacity, surface,
      price, images, error, loading, equipment, equipmentName } = this.state;

    return (

      <div style={{paddingTop: 80}}>
        <Grid container justify="center">
          <Grid item xs={10} md={7}>
            <Divider style={{background: 'purple'}} />
            <SBView>
              <div className={classes.progressLeft}>
                <LText>NUEVO ESPACIO</LText>
                <GraySText>Suspendisse luctus ultrices lectus vel porttitor. In lectus diam, hendrerit a auctor ac, hendrerit a massa.</GraySText>
              </div>
              <CircularProgressbar
                percentage={this.completeness}
                text={`${this.completeness}%`}
                styles={{
                  root: {flex: 1, marginTop: 30},
                  path: { stroke: `rgba(248, 165, 30, ${0.2 + this.completeness / 125})` },
                  text: { fill: `rgba(20, 20, 20, ${0.2 + this.completeness / 100})`, fontSize: '24px', fontWeight: 'bold' },
                }}
              />
            </SBView>

            <TextField
              label="Nombre"
              placeholder="Nombre"
              className={classes.textField}
              value={name}
              onChange={this.handleInputChange('name')}
              margin="normal"
              variant="outlined"
              error={error.name ? true : false}
            />
            { error.name &&
              <ErrorText>{error.name}</ErrorText>
            }

            <TextField
              label="Descripción"
              placeholder="DEFINE TU PROYECTO"
              className={classes.textArea}
              value={description}
              onChange={this.handleInputChange('description')}
              margin="normal"
              variant="outlined"
              multiline
              rows={5}
              error={error.description ? true : false}
            />
            { error.description &&
              <ErrorText>{error.description}</ErrorText>
            }

            <MText>Lugar</MText>
            <AddressPicker
              selectCb={this.handleLocationChange}
              changeCb={this.handleAddressChange}
            />
            { error.address &&
              <ErrorText>{error.address}</ErrorText>
            }

            <Grid container justify="space-between">

              <Grid item xs={12} md={4}>
                <TextField
                  label="Aforo"
                  placeholder="Aforo"
                  className={classes.textField}
                  value={capacity}
                  onChange={this.handleInputChange('capacity')}
                  margin="normal"
                  variant="outlined"
                  error={error.capacity ? true : false}
                />
                { error.capacity &&
                  <ErrorText>{error.capacity}</ErrorText>
                }
              </Grid>

              <Grid item xs={12} md={4}>
                <TextField
                  label="Espacio útil (m2)"
                  placeholder=""
                  className={classes.textField}
                  value={surface}
                  onChange={this.handleInputChange('surface')}
                  margin="normal"
                  variant="outlined"
                  error={error.surface ? true : false}
                />
                { error.surface &&
                  <ErrorText>{error.surface}</ErrorText>
                }
              </Grid>

              <Grid item xs={12} md={4}>
                <TextField
                  label="Precio"
                  placeholder=""
                  className={classes.textField}
                  value={price}
                  onChange={this.handleInputChange('price')}
                  margin="normal"
                  variant="outlined"
                  error={error.price ? true : false}
                />
                { error.price &&
                  <ErrorText>{error.price}</ErrorText>
                }
              </Grid>
            </Grid>

            <Grid container justify="center">
              <Grid item xs={10} md={8} lg={6}>
                <MText>Attributes</MText>
                <AutoCompleteInput
                  value={equipmentName}
                  label="Equipment"
                  placeholder="Type here to add some equipment to your space"
                  onChange={text => this.setState({equipmentName: text})}
                  onSelect={equipmentName => this.onSelectEquipment(equipmentName)}
                  selectedValues={equipment}
                  optionsList={Space__equipmentList}
                />
              </Grid>
            </Grid>

            <Grid container justify="center">
              <Grid item xs={10} md={8} lg={6}>
                <WrapView>
                  {
                    equipment.map(item => (
                      <Chip
                        key={item}
                        label={item}
                        color="default"
                        onDelete={() => this.onRemoveEquipment(item)}
                        variant="default"
                        className={classes.tagChip}
                      />
                    ))
                  }
                </WrapView>
              </Grid>
            </Grid>

            <Divider style={{marginTop: 50}} />
            <MText>Sube 3 fotos de tu espacio</MText>
            <Grid container>

              {
                images.map((image, index) => {
                  return (
                    <Button key={index} className={classes.avatarView}>
                      <label className={classes.imgUploadLabel}>
                        <input
                            type="file"
                            ref={(ref) => this.fileEvent = ref}
                            className={classes.fileInput}
                            onChange={this.onChangePhoto(index)}
                        />

                        <IfView condition={image.imageURL.length === 0}>

                          <AddIcon className={classes.avatarAddIcon}/>

                          <img alt="projectimage" src={image.imageURL} className={classes.avatar} />
                        </IfView>
                      </label>
                    </Button>
                  )
                })
              }

              { error.images &&
                <ErrorText>{error.images}</ErrorText>
              }
            </Grid>

            <Divider style={{marginTop: 50}}/>

            <RightView style={{padding: '40px 0 100px 0'}}>
              <OrangeButton loading={loading} text="Guardar cambios" paddingHorizontal={40} onPress={() => this.onSaveChanges()}/>
            </RightView>
          </Grid>
        </Grid>

      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  me: state.userReducer
})

const mapDispatchToProps = ({
  createSpace: spaceActions.createSpace
})

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(NewResource));
