import * as React from 'react';
import { withRouter } from 'react-router'
import { GrayMText } from '../../styles/global';
import { Container } from '../../styles/invalid';

class InvalidPage extends React.Component {

  render() {    
    return (
      <Container>
        <GrayMText>{"This page doesn't exist"}</GrayMText>
        <GrayMText>
          Please <button onClick={() => window.history.back()}>return</button> 
          to the previous page or visit <button onClick={() => this.props.history.push('/login')}>Home</button>.
        </GrayMText>
      </Container>
    );
  }
}

export default  withRouter(InvalidPage);
