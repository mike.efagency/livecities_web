import * as React from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';

import { NotificationManager } from 'react-notifications';

import { Grid, TextField } from '@material-ui/core';

import { OrangeButton } from '../../components';
import { authActions } from '../../redux/actions';
import { validateEmail, validatePassword } from '../../lib/operators';

import { ErrorText, RightView } from '../../styles/global';

const styles = {
  textField: {
    width: '100%',
  },
};


class LoginForm extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
      error: {},
      loading: false
    }
  }


  handleInputChange = name => event => {

    this.setState({
      [name]: event.target.value,
    });
  };

  // Actual validation fn
  checkField = (key, value, error) => {

    if (value.length === 0) {
      error[key] = 'This field is required'

    } else {

      delete error[key]

      if (key === 'password') {

        // check also that password is valid
        if (validatePassword(value)) {
          delete error[key];

        } else {
          error[key] = `Password must have between 8 and 20 characters among: numbers, lowercase and uppercase letters`;
        }
      }

      if (key === 'email') {

        if (validateEmail(value)) {
          delete error[key];

        } else {
          error[key] = `Enter a valid email`;
        }
      }
    }

    return error;
  }

  // Check fields on blur
  checkThisInputIsValid = key => event => {

    const { error } = this.state;

    this.setState({
      error: this.checkField(key, event.target.value, error)
    })

  }

  // Check input errors
  checkFormErrors = () => {

    const { email, password, error } = this.state;

    let newError = {};

    newError = this.checkField('email', email, error);

    newError = this.checkField('password', password, newError);

    this.setState({
      error: newError
    });

    return Object.keys(newError).length === 0
  }

  onPressLogIn = () => {

    const { email, password } = this.state;

    let noErrorPresent = this.checkFormErrors();

    if (noErrorPresent) {

      this.setState({
        loading: true
      });

      this.props.login({ email, password }, (result, msg = '') => {

        this.setState({
          loading: false
        });

        if (result) {

          // Once the user is created it can be retrieved to the Store
          // this.props.getProfile();

          // Navigate to the home page now as a registered user
          this.props.history.push({
            pathname: '/home',
            state: {
                fromLogin: true
            }
          });

        } else {

          NotificationManager.error(msg, 'Login Failed', 3000, () => {});
        }
      });
    }
  }


  render() {

    const { classes } = this.props;
    const { email, password, error, loading } = this.state;

    return (
      <Grid container justify="center">

        <Grid container justify="center">
          <Grid item xs={10} md={5}>
            <TextField
              error={error.email ? true : false}
              label="Email"
              placeholder="User email"
              className={classes.textField}
              value={email}
              onChange={this.handleInputChange('email')}
              onBlur={this.checkThisInputIsValid('email')}
              margin="normal"
              variant="outlined"
            />

            { error.email &&
              <ErrorText>{error.email}</ErrorText>
            }
          </Grid>
        </Grid>

        <Grid container justify="center">
          <Grid item xs={10} md={5}>
            <TextField
              error={error.password ? true : false}
              label="Password"
              type="password"
              placeholder="Password"
              className={classes.textField}
              value={password}
              onChange={this.handleInputChange('password')}
              onBlur={this.checkThisInputIsValid('password')}
              margin="normal"
              variant="outlined"
            />

            { error.password &&
              <ErrorText>{error.password}</ErrorText>
            }
          </Grid>
        </Grid>

        <Grid container justify="center">
          <Grid item xs={10} md={5}>
            <RightView paddingTop={30}>
              <OrangeButton
                text="Iniciar sesión"
                paddingHorizontal={40}
                loading={loading}
                onPress={this.onPressLogIn}
              />
            </RightView>
          </Grid>
        </Grid>
      </Grid>
    );
  }
}


const mapDispatchToProps = ({
  login: authActions.login,
})

export default withStyles(styles)(connect(null, mapDispatchToProps)(LoginForm));
