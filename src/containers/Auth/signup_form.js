import * as React from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';

import { NotificationManager } from 'react-notifications';

import { Button, Chip, Grid, Switch, TextField } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';

import { AutoSuggestInput, IfView, OrangeButton } from '../../components';
import { authActions } from '../../redux/actions';
import { checkSignupFields, validatePassword } from '../../lib/operators';

import { ErrorText, MText, RightView, WrapView } from '../../styles/global';
import { AvatarSection } from '../../styles/auth';

const styles = theme => ({
  textField: {
    width: '100%',
  },
  avatarView: {
    marginTop: 20,
    marginBottom: 20,
    width: 100,
    height: 100,
    borderRadius: 100,
    border: '1px solid lightgray',
    padding: 0
  },
  avatar: {
    width: 100,
    height: 100,
    borderRadius: 100,
    objectFit: 'cover'
  },
  avatarAddIcon: {
    color: 'green'
  },
  avatarTitle: {

  },
  switchTitle: {
    margin: 0,
    marginRight: 30,
    lineHeight: '24px',
  },
  switchBase: {
    color: 'green',
  },
  submitButtonView: {
    display: 'flex',
    paddingTop: 30,
    paddingBottom: 30,
    [theme.breakpoints.up('md')]: {
      justifyContent: 'flex-end',
    },
    [theme.breakpoints.down('md')]: {
      justifyContent: 'center',
    },
  },
  fileInput: {
    width: 0,
    height: 0
  },
  tagChip: {
    margin: 5
  }
});


class SignUpForm extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      firstName: '',
      lastName: '',
      tagName: '',
      email: '',
      password: '',
      confirm: '',
      tagArray: [],
      accepted: false,
      avatarURL: '',
      selectedFile: {},
      error: {},
      loading: false
    }
  }


  handleInputChange = name => event => {

    this.setState({
      [name]: event.target.value,
    });
  };

  handleSwitchChange = name => event => {

    const { error } = this.state;

    let checkboxValue = event.target.checked;

    this.setState({
      [name]: checkboxValue
    });

    if (checkboxValue) {

      delete error.accepted

    } else {
      error.accepted = `Terms and conditions must be agreed before creating a new account`
    }

    this.setState({ error })
  };

  // Add skill tag
  onSelectSkill = tagName => {

    const { tagArray } = this.state;

    if (tagArray.indexOf(tagName) < 0) {
      this.setState({
        tagArray: [...tagArray, tagName]
      });
    }
  }

  // Remove skill tag
  onRemoveSkill = tagName => {

    let { tagArray } = this.state;

    tagArray = tagArray.filter( tag => (tag !== tagName));

    this.setState({tagArray});
  }

  // Image selected
  onChangePhoto = () => event => {

    const { files } = event.target,
      ValidTypes = ['image/jpeg', 'image/png', 'image/jpg', 'image/JPG', 'image/PNG', 'image/JPEG'];

    if (!files.length) {

      return;
    }

    if (ValidTypes.indexOf(files[0].type) === -1) {

      NotificationManager.error('Invalid File Type', 'Error!', 5000, () => { });

      return;
    }

    let reader = new FileReader();

    reader.onload = () => {

      this.setState({
        avatarURL: reader.result,
        selectedFile: files[0]
      });
    };

    reader.readAsDataURL(files[0]);
  }

  // Check field error
  checkThisInputIsValid = key => event => {

    const { error } = this.state;

    this.setState({
      error: checkSignupFields(key, event.target.value, error)
    });
  }

  // Check passwords match
  checkPasswordConfirmation = key => event => {

    return this.passwordsMatch(key, event.target.value);
  }

  // Password match callback
  passwordsMatch = (key, input) => {

    const { password, confirm, error } = this.state

    let noError = true;

    if (!input || input.length === 0) {

      error[key] = 'This field is required';

    } else {

      delete error[key];
    }

    if (input.length > 0) {

      // check also that password is valid
      if (validatePassword(input)) {
        delete error[key];

      } else {
        error[key] = `Password must have 8 or more (and less than 20) characters among: numbers, lowercase and uppercase letters.`;
        noError = false;
      }
    }

    if (noError && password && password.length && confirm && confirm.length) {

      if (password !== confirm) {

        error.password = 'Password and its confirmation do not match'
        error.confirm = 'Password and its confirmation do not match'
        noError = false

      } else {

        delete error.password
        delete error.confirm
      }
    }

    this.setState({ error })

    return noError
  }

  // Check inputs errors
  checkFormErrors = () => {

    const { firstName, lastName, email, tagArray,
      avatarURL, password, confirm, accepted, error } = this.state;

    let newError = {},
      checkPassword = this.passwordsMatch('password', password),
      checkPasswordConfirm = this.passwordsMatch('confirm', confirm);

    newError = checkSignupFields('firstName', firstName, error);

    newError = checkSignupFields('lastName', lastName, newError);

    newError = checkSignupFields('avatarURL', avatarURL, newError);

    newError = checkSignupFields('email', email, newError);

    newError = checkSignupFields('tagArray', tagArray, newError);

    newError = checkSignupFields('accepted', accepted, newError);

    this.setState({
      error: newError
    });

    return Object.keys(newError).length === 0 &&
      checkPassword && checkPasswordConfirm;
  }

  onSubmitSignUp = () => {

    const { firstName, lastName, email, password, selectedFile, tagArray } = this.state;

    let noErrorPresent = this.checkFormErrors();

    if (noErrorPresent) {

      this.setState({loading: true});

      const postParam = {
        email,
        password,
        first_name: firstName,
        last_name: lastName,
      }

      this.props.signup(postParam, (result, msg = '') => {

        if (result) {

          // Post user skills in tagArray
          tagArray.forEach(skill => {
            this.props.putSkill(skill);
          })

          // Post user image
          this.props.uploadImage(selectedFile, (res, msg = '') => {

            this.setState({
              loading: false
            });

            if (res === 'error') {
              NotificationManager.error(msg, 'Image Upload Failed', 3000, ()=> {});

            } else {
              // Once the user is created it can be retrieved to the Store
              this.props.getProfile();

              // Navigate to the home page now as a registered user
              this.props.history.push('/home');
            }
          })


        } else {

          this.setState({
            loading: false
          });

          NotificationManager.error(msg, 'SignUp Failed', 3000, () => {});
        }
      })

    }
  }


  render() {

    const { classes, getSkills } = this.props;

    const { tagName, tagArray, error, loading, firstName, lastName,
      email, password, confirm, avatarURL, accepted } = this.state;

    return (
      <Grid container justify="center">
        <Grid item xs={10} md={6}>
          <Grid container justify="space-between">
            <Grid item xs={5}>
                <TextField
                  error={error.firstName ? true : false}
                  label="Nombre"
                  placeholder="Nombre should not be empty"
                  className={classes.textField}
                  value={firstName}
                  onChange={this.handleInputChange('firstName')}
                  onBlur={this.checkThisInputIsValid('firstName')}
                  margin="normal"
                  variant="outlined"
                />

                { error.firstName &&
                  <ErrorText>{error.firstName}</ErrorText>
                }
            </Grid>
            <Grid item xs={5}>
                <TextField
                  error={error.lastName ? true : false}
                  label="Apellidos"
                  placeholder="Apellidos should not be empty"
                  className={classes.textField}
                  value={lastName}
                  onChange={this.handleInputChange('lastName')}
                  onBlur={this.checkThisInputIsValid('lastName')}
                  margin="normal"
                  variant="outlined"
                />

                { error.lastName &&
                  <ErrorText>{error.lastName}</ErrorText>
                }
            </Grid>
          </Grid>
        </Grid>

        <Grid container justify="center">
          <Grid item xs={10} md={6}>
            <TextField
              error={error.email ? true : false}
              label="Email"
              placeholder="Input valid email address"
              className={classes.textField}
              value={email}
              onChange={this.handleInputChange('email')}
              onBlur={this.checkThisInputIsValid('email')}
              margin="normal"
              variant="outlined"
            />

            { error.email &&
              <ErrorText>{error.email}</ErrorText>
            }
          </Grid>
        </Grid>

        <Grid item xs={10} md={6}>
          <Grid container justify="space-between">
            <Grid item xs={5}>
              <TextField
                error={error.password ? true : false}
                label="Contraseña"
                placeholder="At least 8 characters, 1 uppercase, 1 lowercase, 1 number"
                className={classes.textField}
                value={password}
                type="password"
                onChange={this.handleInputChange('password')}
                onBlur={this.checkPasswordConfirmation('password')}
                margin="normal"
                variant="outlined"
              />

              { error.password &&
                <ErrorText>{error.password}</ErrorText>
              }
            </Grid>
            <Grid item xs={5}>
              <TextField
                error={error.confirm ? true : false}
                label="Confirm Password"
                placeholder="Confirm your password"
                className={classes.textField}
                value={confirm}
                type="password"
                onChange={this.handleInputChange('confirm')}
                onBlur={this.checkPasswordConfirmation('confirm')}
                margin="normal"
                variant="outlined"
              />

              { error.confirm &&
                <ErrorText>{error.confirm}</ErrorText>
              }
            </Grid>
          </Grid>
        </Grid>

        <Grid container justify="center">
          <Grid item xs={10} md={6}>
            <AutoSuggestInput
              value={tagName}
              label="Habilidades"
              placeholder="Type here to add some skills to your profile"
              onChange={text => this.setState({tagName: text})}
              onSelect={tagName => this.onSelectSkill(tagName)}
              selectedValues={tagArray}
              getSkills={getSkills}
            />
          </Grid>
        </Grid>

        <Grid container justify="center">
          <Grid item xs={10} md={6}>

            { ( error.tagArray ||
              tagArray.length < 3 ) &&
              <ErrorText>You should set 3 skills at least</ErrorText> }

            <WrapView>
              {
                tagArray.map(tag => (
                  <Chip
                    key={tag}
                    label={tag}
                    color="default"
                    onDelete={() => this.onRemoveSkill(tag)}
                    variant="default"
                    className={classes.tagChip}
                  />
                ))
              }
            </WrapView>
          </Grid>
        </Grid>

        <Grid container justify="center">
          <Grid item xs={10} md={6}>
            <AvatarSection>
              <Grid container justify="center">
                <Grid item xs={10} md={5}>
                  <MText justify="center">Sube tu mejor foto</MText>
                  <center>
                    <Button className={classes.avatarView}>
                      <label>
                        <input type="file" ref={ref => this.fileEvent = ref} className={classes.fileInput} onChange={this.onChangePhoto()}/>

                        <IfView condition={avatarURL.length === 0}>

                          <AddIcon className={classes.avatarAddIcon}/>

                          <img alt="avatar" src={this.state.avatarURL} className={classes.avatar} />
                        </IfView>
                      </label>
                    </Button>

                    { error.avatarURL &&
                      <ErrorText>{error.avatarURL}</ErrorText>
                    }
                  </center>
                </Grid>

                <Grid item xs={8} md={6}>
                  <MText>Al registrarme acepto los términos y condiciones, y también, la política de privacidad del servicio.</MText>

                  { error.accepted &&
                    <ErrorText>{error.accepted}</ErrorText>
                  }
                </Grid>
                <Grid item xs={2} md={1}>
                  <Switch
                    checked={accepted}
                    onChange={this.handleSwitchChange('accepted')}
                    value="accepted"
                    color="primary"
                  />
                </Grid>
              </Grid>
            </AvatarSection>
          </Grid>
        </Grid>

        <Grid container justify="center">
          <Grid item xs={10} md={6}>
            <RightView paddingTop={30}>
              <OrangeButton
                text="Empezar ahora"
                loading={loading}
                paddingHorizontal={40}
                onPress={this.onSubmitSignUp}
              />
            </RightView>
          </Grid>
        </Grid>
      </Grid>
    );
  }
}


const mapStateToProps = (state) => ({
  homeReducer: state.homeReducer
});

const mapDispatchToProps = ({
  signup: authActions.signup,
  uploadImage: authActions.uploadImage,
  getProfile: authActions.getProfile,
  getSkills: authActions.getSkills,
  putSkill: authActions.putSkill
})

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(SignUpForm));
