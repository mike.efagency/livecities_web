import * as React from 'react';
import { connect } from 'react-redux';

import LoginForm from './login_form';

import { WhiteLText, WhiteMText, Bold } from '../../styles/global';
import { TopWrapper, W60 } from '../../styles/auth';


class Login extends React.Component {

  componentDidMount() {

    if (this.props.loggedIn) {
      this.props.history.push('/home')
    }
  }

  render() {

    const { history } = this.props;

    return (
      <div>
        <TopWrapper>
          <Bold><WhiteLText justify="center">WELCOME ONBOARD!</WhiteLText></Bold>
          <center>
            <W60>
              <WhiteMText justify="center">
                Nunc maximus, nulla sit amet consectetur porttitor, lorem ipsum dolor nunc maximus, nulla sit amet consectetur porttitor
              </WhiteMText>
            </W60>
          </center>
        </TopWrapper>
        <LoginForm history={history}/>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  loggedIn: state.userReducer.loggedIn
});

export default connect(mapStateToProps)(Login);
