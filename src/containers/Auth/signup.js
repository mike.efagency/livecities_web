import * as React from 'react';
import { connect } from 'react-redux';

import SignUpForm from './signup_form';

import { authActions } from '../../redux/actions';

import { TopWrapper, W60 } from '../../styles/auth';
import { WhiteLText, WhiteMText, Bold} from '../../styles/global';


class SignUp extends React.Component {

  componentDidMount() {

    if (this.props.loggedIn) {
      this.props.history.push('/login');

      return
    }

    this.props.getSkills('', () => {});
  }

  render() {
    const { history } = this.props;
    return (
      <div>
        <TopWrapper>
          <Bold><WhiteLText justify="center">WELCOME ONBOARD!</WhiteLText></Bold>
          <center>
            <W60>
              <WhiteMText justify="center">
                Nunc maximus, nulla sit amet consectetur porttitor, lorem ipsum dolor nunc maximus, nulla sit amet consectetur porttitor
              </WhiteMText>
            </W60>
          </center>
        </TopWrapper>
        <SignUpForm history={history}/>
      </div>
    );
  }
}


const mapStateToProps = (state) => ({
  loggedIn: state.userReducer.loggedIn
});

const mapDispatchToProps = ({
  getSkills: authActions.getSkills
})

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);
