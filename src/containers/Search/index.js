import * as React from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import DoneIcon from '@material-ui/icons/Done';
import AddIcon from '@material-ui/icons/Add';
import CircularProgress from '@material-ui/core/CircularProgress';
import { Icon, Divider, Radio, RadioGroup, FormControlLabel, Chip } from '@material-ui/core';
import { GrayMText, CenterRow, SText, WrapView, Row, RightView } from '../../styles/global';
import { SearchInput, CustomSlider, CustomInput, OrangeButton, SwitchIOS } from '../../components';
import Resources from './resources';
import Projects from './projects';
import { Colors } from '../../lib/theme';
import { searchActions } from '../../redux/actions';

const ProjectTags = ['Entertainment', 'Shopify', 'Life Cycle'];
const ProjectSortTypes = ['Sort by', 'Start Date', 'Price', 'Name'];

const styles = {
  textField: {
    width: '100%'
  },
  filterButton: {
    '&:hover': {
      backgroundColor: 'transparent',
    },
  },
  radioGroup: {
    paddingLeft: 20,
  },
  radio: {
    width: 8,
    height: 8,
    borderRadius: 24,
    border: '8px solid ' + Colors.green,
    backgroundColor: 'white'
  },
  rootRadio: {
    width: 24,
    height: 24,
    color: Colors.lightgray
  },
  label: {
    color: Colors.lightgray
  },
  tag: {
    margin: 5
  },
  progressBar: {
    marginRight: 20
  }
}

const types = ['project', 'resource', 'all'];

class Search extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showMap: false,
      showFilter: false,
      type: 'all', // resource, task, todo(both)
      selectedTagArray: [],
      distance: 0,
      sortBy: "Sort by",
      search: '',
    }
  }

  componentDidMount() {
    const { match, history, location, me} = this.props;
    const type = match.params.type;
    if(type === undefined || types.indexOf(type) < 0){
      this.setState({type: 'all'});
      history.push('/search');
    }
    else this.setState({type});

    this.props.getSearchResultByClass(me.token, type);
    if(location.state !== undefined) this.setState({search: location.state.search});
    else {
      this.props.setSearchText('');
    }
  }

  onSearch = () => {
    // this.props.setSearchText(this.state.search);
    this.props.getSearchResultByText(this.state.search);
  }

  toggleFilterView = () => {
    this.setState({showFilter: !this.state.showFilter});
  }

  handleInputChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  onChangeType = (e) => {
    const type = e.target.value;
    this.setState({type});
    this.props.history.push('/search/' + type);
  }

  onSelectTag = (tag) => {
    const selectedTagArray = this.state.selectedTagArray;
    const index  = selectedTagArray.indexOf(tag);
    if(index < 0) selectedTagArray.push(tag);
    else selectedTagArray.splice(index, 1);
    this.setState({selectedTagArray});
  }

  handleDistanceChange = (value) => {
    this.setState({ distance: value });
  };

  loadMoreByClass = (type) => {
    this.props.loadMoreByClass(type, (res) => {
      if(res === 'success') this.forceUpdate();
    })
  }

  render() {
    const { classes, resources, projects, searchText, searching } = this.props;
    const { showMap, type, showFilter, selectedTagArray, distance, sortBy, search } = this.state;
    const visibleResource = type === 'resource' || type === 'all';
    const visibleTask = type === 'project' || type === 'all';
    return (
      <div style={{paddingTop: 40}}>
        <Grid container justify="center">
          <Grid item xs={11} sm={9}>
            <SearchInput placeholder="search" value={search} onChange={this.handleInputChange("search")} onSearch={() => this.onSearch()} />
            <Grid container justify="space-between">
              <Button className={classes.filterButton} onClick={this.toggleFilterView}>
                <CenterRow>
                  <GrayMText style={{marginRight: 20}}>Filtros avanzados</GrayMText>
                  <Icon color='primary'>tune</Icon>
                </CenterRow>
              </Button>
              <CenterRow>
                <GrayMText>Ver mapa</GrayMText>
                <SwitchIOS value={true} onChange={(checked) => this.setState({showMap: checked})} />
              </CenterRow>
            </Grid>
            {showFilter &&
              <Grid container>
                <Divider />
                <Grid item xs={12} md={2}>
                  <Grid container>
                    <Grid item xs={6} md={12}>
                      <SText>Tipo de resultados:</SText>
                      <RadioGroup
                        className={classes.radioGroup}
                        value={type}
                        onChange={this.onChangeType}
                      >
                        {types.map((type) => (
                          <FormControlLabel
                            key={type}
                            classes={{label: classes.label}}
                            value={type}
                            control={<Radio classes={{root: classes.rootRadio}} checkedIcon={<div className={classes.radio}/>} />}
                            label={type.toUpperCase()}
                          />
                        ))}
                      </RadioGroup>
                    </Grid>
                    <Grid item xs={6} md={12}>
                      <SText>Lorem est:</SText>
                      <RadioGroup
                        className={classes.radioGroup}
                        value={type}
                        onChange={this.onChangeType}
                      >
                        {types.map((type) => (
                          <FormControlLabel
                            key={type}
                            classes={{label: classes.label}}
                            value={type}
                            control={<Radio classes={{root: classes.rootRadio}} checkedIcon={<div className={classes.radio}/>} />}
                            label={type.toUpperCase()}
                          />
                        ))}
                      </RadioGroup>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid item xs={12} md={10}>
                  {visibleResource &&
                    <div>
                      <SText>De estas categorías:</SText>
                      <WrapView>
                        {
                          ProjectTags.map((tag) => {
                            const selected = selectedTagArray.indexOf(tag) > -1;
                            return(
                              <Chip
                                key={tag}
                                label={tag}
                                clickable
                                color={selected ? "primary" : 'default'}
                                onClick={() => this.onSelectTag(tag)}
                                onDelete={() => this.onSelectTag(tag)}
                                deleteIcon={selected ? <DoneIcon /> : <AddIcon />}
                                variant="outlined"
                                className={classes.tag}
                              />
                            )
                          })
                        }
                      </WrapView>
                    </div>
                  }
                  {visibleTask &&
                    <CustomSlider
                      label="Buscar cercanos:"
                      value={distance}
                      onChange={this.handleDistanceChange}
                      maxValue={50}
                    />
                  }
                  <Grid container>
                    <Grid item xs={12} md={6}>
                      <div style={{paddingTop: 20}}>
                        <CustomInput
                          type="select"
                          label=""
                          value={sortBy}
                          onChange={this.handleInputChange('sortBy')}
                          options={ProjectSortTypes}
                          disableFirst
                        />
                      </div>
                    </Grid>
                  </Grid>
                </Grid>
                <Divider />
              </Grid>
            }
            {searching &&
              <Row><CircularProgress className={classes.progressBar} size={16}/><GrayMText> Searching...</GrayMText></Row>
            }
            {visibleResource && !searching &&
              <div>
                <Resources resources={resources} showMap={showMap} distance={distance} sortBy={sortBy} search={searchText}/>
                <RightView><OrangeButton text="Load More" onPress={() => this.loadMoreByClass('resource')} /></RightView>
              </div>
            }
            {visibleTask && !searching &&
              <div style={{paddingBottom: 50}}>
                <Projects projects={projects} showMap={showMap} distance={distance} sortBy={sortBy} search={searchText}/>
                <RightView><OrangeButton text="Load More" onPress={() => this.loadMoreByClass('task')} /></RightView>
              </div>
            }
          </Grid>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  me: state.userReducer,
  resources: state.homeReducer.resources,
  projects: state.homeReducer.projects,
  searchText: state.homeReducer.searchText,
  searching: state.homeReducer.searching
});

const mapDispatchToProps = ({
  getSearchResultByClass: searchActions.getSearchResultByClass,
  setSearchText: searchActions.setSearchText,
  getSearchResultByText: searchActions.getSearchResultByText,
  loadMoreByClass: searchActions.loadMoreByClass
})

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(Search));
