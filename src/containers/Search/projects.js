import * as React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import MarkerClusterer from 'react-google-maps/lib/components/addons/MarkerClusterer';
import { Marker, InfoWindow } from 'react-google-maps';
import { LightGrayMText, MText } from '../../styles/global';
import { IfView } from '../../components';
import { compareObjectValues } from '../../lib/operators';
import { Colors } from '../../lib/theme';
import ProjectCard from '../Home/ProjectCard';
import { MapWrapper } from '../../styles/search';
import { MyGoogleMap } from '../../components/MyGoogleMap';

const styles = {
  button: {
    width: '100%'
  }
}

const markers = [
  { lat: 25.0391667, lng: 121.525, title: 'Project1' },
  { lat: 25.0391567, lng: 121.5252, title: 'Project2' }
];

class Projects extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: props.name,
      email: props.email,
      showInfoIndex: -1
    }
  }

  handleInputChange = (name) => event => {
    this.setState({[name]: event.target.value})
  };

  handleClose = () => {
    this.props.onClose();
  };

  filter = (projects, search) => {
    let res = [];
    projects.map((project) => {
      if(JSON.stringify(project).toLowerCase().indexOf(search.toLowerCase()) < 0) return false;
      res.push(project);
      return true;
    })
    return res;
  }

  onPressItem = () => {
    console.log('Pressed a project');
  }

  getSortKey = (sortBy) => {
    switch(sortBy) {
      case 'Start Date':
        return 'start_date';
      case 'Price':
        return 'owner';
      case 'Name':
        return 'name';
      default:
        return '';
    }
  }

  showInfo = (index) => {
    this.setState({showInfoIndex: index});
  }

  closeInfo = () => {
    this.setState({showInfoIndex: -1});
  }

  render() {
    const { showInfoIndex } = this.state;
    const { showMap, projects, search, sortBy } = this.props;
    const sortkey = this.getSortKey(sortBy);
    const filteredProjects = this.filter(projects, search);
    return (
      <Grid container>
        <MText><span style={{color: Colors.green}}>{filteredProjects.length}</span> Projects</MText>

        <IfView condition={showMap}>
          <Grid container>
            <Grid item xs={12} md={7}>
              <IfView condition={filteredProjects.length === 0}>
                <LightGrayMText>No results</LightGrayMText>
                <Grid container>
                {
                  filteredProjects.sort(compareObjectValues(sortkey)).map((project) => {
                    if(project.id === undefined) return null;
                    return(
                      <Grid key={project.id} item xs={12} md={6} lg={4}>
                        <ProjectCard project={project} onPressItem={() => this.onPressItem()} />
                      </Grid>
                    )
                  })
                }
                </Grid>
              </IfView>
            </Grid>
            <Grid item xs={12} md={5}>
              {filteredProjects.length > 0 &&
                <MapWrapper>
                  <MyGoogleMap center={{ lat: 25.0391667, lng: 121.525 }} zoom={8}>
                    <MarkerClusterer>
                      {
                        markers.map((marker, index) => {
                          return(
                            <Marker
                              key={index}
                              position={{ lat: marker.lat, lng: marker.lng}}
                              onClick={() => this.showInfo(index)}
                            >
                              { showInfoIndex === index &&
                              <InfoWindow  onCloseClick={() => this.closeInfo()}>
                                <div>
                                  <div>{marker.title}</div>
                                </div>
                              </InfoWindow>}
                            </Marker>
                          )
                        })
                      }
                    </MarkerClusterer>
                  </MyGoogleMap>
                </MapWrapper>
              }
            </Grid>
          </Grid>
          <Grid container>
            <IfView condition={filteredProjects.length === 0}>
              <LightGrayMText justify="center">No results</LightGrayMText>
              <Grid container>
                {
                  filteredProjects.sort(compareObjectValues(sortkey)).map((project) => {
                    if(project.id === undefined) return null;
                    return(
                      <Grid key={project.id} item xs={12} md={6} lg={3}>
                        <ProjectCard project={project} onPressItem={() => this.onPressItem()} />
                      </Grid>
                    )
                  })
                }
              </Grid>
            </IfView>
          </Grid>
        </IfView>
      </Grid>
    );
  }
}

Projects.propTypes = {
  projects: PropTypes.array.isRequired,
  showMap: PropTypes.bool.isRequired,
  search: PropTypes.string.isRequired,
  sortBy: PropTypes.string.isRequired
}

export default withStyles(styles)(Projects);
