import * as React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import { InfoWindow, Marker } from 'react-google-maps';

import { LightGrayMText, MText } from '../../styles/global';
import { IfView } from '../../components';
import { Colors } from '../../lib/theme';
import { compareObjectValues } from '../../lib/operators';
import ResourceItem from '../Home/ResourceCard';
import { MapWrapper } from '../../styles/search';
import { MyGoogleMap } from '../../components/MyGoogleMap';

const { MarkerClusterer } = require("react-google-maps/lib/components/addons/MarkerClusterer");

const styles = {

}

const markers = [
  { lat: 25.0391667, lng: 121.525, title: 'Resource1' },
  { lat: 25.0391567, lng: 121.5252, title: 'Resource2' }
];

class Resources extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: props.name,
      email: props.email,
      showInfoIndex: -1
    }
  }

  handleInputChange = (name) => event => {
    this.setState({[name]: event.target.value})
  };

  handleClose = () => {
    this.props.onClose();
  };

  getSortKey = (sortBy) => {
    switch(sortBy) {
      case 'Start Date':
        return 'created';
      case 'Price':
        return 'owner';
      case 'Name':
        return 'name';
      default:
        return '';
    }
  }

  filter = (resources, search) => {
    let res = [];
    resources.map((resource) => {
      if(JSON.stringify(resource).toLowerCase().indexOf(search.toLowerCase()) < 0) return false;
      res.push(resource);
      return true;
    })
    return res;
  }

  showInfo = (index) => {
    this.setState({showInfoIndex: index});
  }

  closeInfo = () => {
    this.setState({showInfoIndex: -1});
  }

  render() {
    const { showInfoIndex } = this.state;
    const { sortBy, resources, showMap, search } = this.props;
    const sortkey = this.getSortKey(sortBy);
    const filteredResources = this.filter(resources, search);
    return (
      <Grid container>
        <MText><span style={{color: Colors.green}}>{filteredResources.length}</span> Resources</MText>

        <IfView condition={showMap}>
          <Grid container>
            <Grid item xs={12} md={7}>
              <IfView condition={filteredResources.length === 0}>
                <LightGrayMText>No results</LightGrayMText>
                <Grid container>
                {
                  filteredResources.sort(compareObjectValues(sortkey)).map((resource) => {
                    return (
                      <Grid key={resource.id} item xs={12} md={6} lg={4}>
                        <ResourceItem key={resource.id} resource={resource} onPressItem={() => this.onPressCard()}/>
                      </Grid>
                    )
                  })
                }
                </Grid>
              </IfView>
            </Grid>
            <Grid item xs={12} md={5}>
              {filteredResources.length > 0 &&
                <MapWrapper>
                  <MyGoogleMap center={{ lat: 25.0391667, lng: 121.525 }} zoom={8}>
                    <MarkerClusterer>
                      {
                        markers.map((marker, index) => {
                          return(
                            <Marker
                              key={index}
                              position={{ lat: marker.lat, lng: marker.lng}}
                              onClick={() => this.showInfo(index)}
                            >
                              { showInfoIndex === index &&
                              <InfoWindow  onCloseClick={() => this.closeInfo()}>
                                <div>
                                  <div>{marker.title}</div>
                                </div>
                              </InfoWindow>}
                            </Marker>
                          )
                        })
                      }
                    </MarkerClusterer>
                  </MyGoogleMap>
                </MapWrapper>
              }
            </Grid>
          </Grid>
          <Grid container>
            <IfView condition={filteredResources.length === 0}>
              <LightGrayMText justify="center">No results</LightGrayMText>
              <Grid container>
              {
                filteredResources.sort(compareObjectValues(sortkey)).map((resource) => {
                  return (
                    <Grid key={resource.id} item xs={12} md={6} lg={3}>
                      <ResourceItem key={resource.id} resource={resource} onPressItem={() => this.onPressCard()}/>
                    </Grid>
                  )
                })
              }
              </Grid>
            </IfView>
          </Grid>
        </IfView>
      </Grid>
    );
  }
}

Resources.propTypes = {
  resources: PropTypes.array.isRequired,
  showMap: PropTypes.bool.isRequired,
  sortBy: PropTypes.string.isRequired,
  search: PropTypes.string.isRequired
}

export default withStyles(styles)(Resources);
