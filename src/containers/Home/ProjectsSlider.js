import * as React from 'react';
import { withStyles } from '@material-ui/core/styles';

import PropTypes from 'prop-types';

import Slider from "react-slick";
import ProjectCard from './ProjectCard';

const styles = {
  button: {
    width: '100%',
    padding: 10,
    textTransform: 'none'
  },
}


class ProjectsSlider extends React.Component {

  render() {

    const { screenWidth, projects } = this.props;

    const settings = {
      dots: false,
      infinite: true,
      speed: 500,
      slidesToShow: screenWidth < 768 ? 1 : 3,
      slidesToScroll: screenWidth < 768 ? 1 : 3,
      swipe: screenWidth < 768,
    };

    let projectList = projects;

    if (projects.length < 3 && screenWidth >= 768) {

      projectList = projectList.push({}).push({}).slice(0, 3)
    }

    return (
      <Slider {...settings}>
        {
          projectList.map((project, index) => (
            <ProjectCard key={index} onClick={() => this.props.onClick(project)} project={project} />
          ))
        }
      </Slider>
    );
  }
}


ProjectsSlider.propTypes = {
  onClick: PropTypes.func.isRequired,
  projects: PropTypes.array.isRequired
};

export default withStyles(styles)(ProjectsSlider);
