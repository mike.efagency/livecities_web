import * as React from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';

import { Button, Grid, Link } from '@material-ui/core';

import { SearchInput, OrangeButton, IfView } from '../../components';
import { authActions, homeActions, searchActions, spaceActions } from '../../redux/actions';
import { Colors } from '../../lib/theme';

import ResourceCard from './ResourceCard';
import ProjectsSlider from './ProjectsSlider';

import { WhiteMText, WhiteLText, CenterColumn, MText, SBView, LightGrayMText } from '../../styles/global';
import { TopWrapper, SearchWrapper, BottomWrapper, BottomBackground } from '../../styles/home';

const styles = {
  headerFillButton: {
    background: Colors.blue,
    color: 'white',
    borderRadius: 36,
    margin: 5
  },
  headerOutlineButton: {
    color: 'white',
    borderColor: 'white',
    borderRadius: 36,
    margin: 5
  },
};


class Home extends React.Component {

  constructor(props) {

    super(props);

    this.state = {
      search: ''
    }
  }


  componentDidMount() {

    this.props.getHomeTotalResources()

    this.props.getHomeResources()

    this.props.getHomeProjects()

    this.retrieveUserProfile()
  }

  retrieveUserProfile() {
    let isFromLogin = false;

    const { location, me } = this.props;

    try {

      isFromLogin = location.state.fromLogin;

      if ((isFromLogin || !me.email) && me.loggedIn) {

        this.props.getProfile();
      }

    } catch (e) {
      console.log(e.toString());
    }

  }

  handleInputChange = name => event => {

    this.setState({
      [name]: event.target.value,
    });
  };

  onSearch = () => () => {

    this.props.setSearchText(this.state.searchText);

    this.props.history.push({
      pathname: '/search',
      state: { search: this.state.searchText }
    });
  }

  addNewResource = () => {

    if (this.props.me.loggedIn) {
      this.props.history.push('/resource/new');

    } else {
      this.props.history.push('/login');
    }
  }

  addNewProject = () => {

    if (this.props.me.loggedIn) {
      this.props.history.push('/project/new');

    } else {
      this.props.history.push('/login');
    }
  }

  toResourcePage = id => {

    this.props.history.push(`/resource/${id}`);
  }

  toProjectPage = project => {

    // this.props.setSelectedProject(project);
    //
    // this.props.history.push('/project');

    console.log('Navigate to this project:', project)
  }


  render() {

    const { search } = this.state;

    const { classes, screenWidth, resources, projects, totalResources } = this.props;

    return(
      <div>

        <TopWrapper>
          <Grid container style={{flex: 1}}>
            <CenterColumn>
              <Grid item xs={10} sm={6} md={5} lg={4}>
                <Grid container justify="center">
                  <Grid item xs={12}><WhiteLText justify="center">Colabora en la resolución</WhiteLText></Grid>
                  <Grid item xs={12}><WhiteLText justify="center">de retos en tu comunidad</WhiteLText></Grid>
                </Grid>
                <Grid container justify="space-around">
                  <Button variant="contained" color="primary" className={classes.headerFillButton} onClick={this.addNewResource}>Crea un proyecto</Button>
                  <Button variant="outlined" className={classes.headerOutlineButton} onClick={this.addNewProject}>Publica un recurso</Button>
                </Grid>
              </Grid>
            </CenterColumn>
          </Grid>

          <div style={{flex: 1}}>
            <SearchWrapper>
              <Grid container justify="center">
                <Grid item xs={10} sm={5} lg={5}>
                  <WhiteMText justify="center">Busca retos, proyectos, recursos o espacios disponibles. Toda la comunidad a tu alcance lorem ipsum dolor sit amet.</WhiteMText>
                </Grid>
                <Grid item xs={11} lg={9}>
                  <SearchInput
                    placeholder="Prueba con “Sala polivalante”"
                    value={search}
                    onChange={this.handleInputChange('search')}
                    onSearch={this.onSearch}
                  />
                </Grid>
              </Grid>
            </SearchWrapper>
          </div>
        </TopWrapper>

        <Grid container justify="center">

          <Grid item xs={11} md={9}>

            <MText justify="center">Hay {totalResources} recursos disponibles en tu comunidad</MText>

            <SBView>
              <MText>Recursos más solicitados</MText>
              {
                resources.length &&
                <Link href="/search/resource">{`Ver todos >`}</Link>
              }
            </SBView>

            <IfView condition={resources.length > 0}>
              <Grid container>
              {
                resources.map(resource => (
                  <Grid key={resource.id} item xs={12} md={6} lg={3}>
                    <ResourceCard key={resource.id} resource={resource} onClick={() => this.toResourcePage(resource.id)}/>
                  </Grid>
                ))
              }
              </Grid>

              <LightGrayMText justify="center">No hay recursos para mostrar</LightGrayMText>
            </IfView>
          </Grid>

          <Grid item xs={10} md={9}>

            <SBView>
              <MText>Retos publicados esta semana</MText>
              {
                projects.length &&
                <Link href="/search/project">{'Ver todos >'}</Link>
              }
            </SBView>

            <IfView condition={projects.length > 0}>
              <ProjectsSlider projects={projects} onClick={this.toProjectPage} screenWidth={screenWidth}/>

              <LightGrayMText justify="center">No hay proyectos para mostrar</LightGrayMText>
            </IfView>
          </Grid>

        </Grid>

        <BottomWrapper>
          <BottomBackground>
            <WhiteMText>Publica tu recurso lorem ipsum doloer est</WhiteMText>
            <OrangeButton text="Empezar ahora" paddingHorizontal={40} onPress={() => this.addNewResource()}/>
          </BottomBackground>
        </BottomWrapper>

      </div>
    )
  }
}


const mapStateToProps = (state) => ({
  screenWidth: state.screenReducer.width,
  me: state.userReducer,
  resources: state.homeReducer.resources,
  projects: state.homeReducer.projects,
  totalResources: state.homeReducer.totalResources
});

const mapDispatchToProps = ({
  getHomeResources: homeActions.getHomeResources,
  getHomeProjects: homeActions.getHomeProjects,
  getHomeTotalResources: homeActions.getHomeTotalResources,
  setSearchText: searchActions.setSearchText,
  setSelectedSpace: spaceActions.setSelectedSpace,
  getProfile: authActions.getProfile
})

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(Home));
