import * as React from 'react';
import { withStyles } from '@material-ui/core/styles';

import PropTypes from 'prop-types';

import Button from '@material-ui/core/Button';
import StarRatingComponent from 'react-star-rating-component';

import { Colors } from '../../lib/theme';

import { CardWrapper, CardImage, Dot, AuthorName } from '../../styles/home';
import { Row, SText, LightGraySText } from '../../styles/global';

const styles = {
  button: {
    width: '100%',
    textTransform: 'none'
  },
}

const kinds = {
  Unknown: 'yellow',
  Place: 'green'
}

class ResourceCard extends React.Component {

  render() {

    const { classes, resource } = this.props;

    return (
      <Button onClick={() => this.props.onClick(resource)} className={classes.button}>
        <CardWrapper>
          {/* TODO: use the resource image from API or a placeholder */}
          <CardImage src={'https://placeimg.com/640/480/tech'} />

          <Row>
            <Dot bgColor={kinds[resource.kind]} />
            <SText style={{marginRight: 10}}>{resource.kind.toUpperCase()}</SText>
            <LightGraySText>{resource.is_available ? 'DISPONIBLE' : 'NO DISPONIBLE'}</LightGraySText>
          </Row>

          <SText style={{height: 45, overflow: 'hidden'}}>
            {resource.name}
          </SText>
          <SText>de <AuthorName>{resource.owner.name}</AuthorName></SText>

          <Row>
            {
              resource.ratingsCount > 0 &&
              <StarRatingComponent
                name="rate1"
                starCount={5}
                value={resource.rating}
                editing={true}
                emptyStarColor={Colors.gray}
              />
            }
            <SText>{resource.ratingsCount === 0 ? 'Sin valoración' : resource.ratingsCount}</SText>
          </Row>
        </CardWrapper>
      </Button>
    );
  }
}

ResourceCard.propTypes = {
  onClick: PropTypes.func.isRequired,
  resource: PropTypes.object.isRequired
};

export default withStyles(styles)(ResourceCard);
