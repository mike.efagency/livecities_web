import * as React from 'react';
import { withStyles } from '@material-ui/core/styles';

import PropTypes from 'prop-types';

import Button from '@material-ui/core/Button';

import { ProgressBar } from '../../components';

import { CardWrapper, CardImage } from '../../styles/home';
import { SText, SBView, LightGraySText } from '../../styles/global';

const styles = {
  button: {
    width: '100%',
    padding: 10,
    textTransform: 'none',
    height: '100%'
  },
}


class ProjectCard extends React.Component {

  render() {

    const { classes, project } = this.props;

    return (
      <Button onClick={this.props.onClick} className={classes.button}>
        <CardWrapper>
          {/* TODO: use the resource image from API or a placeholder */}
          <CardImage src={'https://placeimg.com/640/480/tech'} />

          <SText>{project.name}</SText>
          <SText style={{height: 100, overflow: 'hidden'}}>{project.description}</SText>

          <SBView>
            <LightGraySText>{project.owner === undefined ? '' : project.owner.name}</LightGraySText>

            {/* TODO: completed progress bar is not longer needed in this format. Shall we remove this? */}
            <ProgressBar value={project.completed_count === undefined ? 0 : project.completed_count} />
          </SBView>
        </CardWrapper>
      </Button>
    );
  }
}


ProjectCard.propTypes = {
  onClick: PropTypes.func.isRequired,
  project: PropTypes.object.isRequired
};

export default withStyles(styles)(ProjectCard);
