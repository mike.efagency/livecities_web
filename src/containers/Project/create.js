import * as React from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';

import { NotificationManager } from 'react-notifications';
import CircularProgressbar from 'react-circular-progressbar';
import Geocode from 'react-geocode';

import { Button, Grid, Divider, TextField } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';

import { AddressPicker, IfView, OrangeButton, CustomDatePicker, CustomInput } from '../../components';
import { projectActions } from '../../redux/actions';
import {GoogleMapKey } from '../../lib/constants';
import { getHash } from '../../lib/operators';
import { Colors } from '../../lib/theme';

import NewActivity from '../Profile/new_activity';

import { SBView, LightGraySText, LText, GraySText, MText, ErrorText, RightView } from '../../styles/global';
import { ActivityCard, AddActivityCard } from '../../styles/profile';

const styles = theme => ({
  button: {
    width: '100%',
    padding: 10
  },
  textField: {
    width: '100%'
  },
  textArea: {
    width: '100%',
    minHeight: 100
  },
  progressLeft: {
    [theme.breakpoints.down('sm')]: {
      flex: 2,
      marginRight: 10
    },
    [theme.breakpoints.up('sm')]: {
      flex: 4,
      marginRight: 80
    },
  },
  circle: {
    flex: 1,
    marginLeft: 60
  },
  avatarView: {
    marginTop: 20,
    marginBottom: 20,
    width: 100,
    height: 100,
    borderRadius: 100,
    border: '1px solid lightgray',
    padding: 0
  },
  avatar: {
    width: 100,
    height: 100,
    borderRadius: 100,
  },
  avatarAddIcon: {
    color: 'green'
  },
  fileInput: {
    width: 0,
    height: 0
  },
  datePicker: {
    width: '100%',
    border: '1px solid #ced4da',
    borderRadius: 4,
    height: 40,
    overflow: 'hidden',
    margin: 0
  },
  datePickerInput: {
    minHeight: 60,
    marginTop: -10,
    paddingLeft: 12,
    color: Colors.gray
  },
  datePickerIcon: {
    position: 'absolute',
    right: 10,
    top: 5,
    bottom: 5,
    fontSize: 30,
    color: Colors.green
  },
  activityPriceView: {
    [theme.breakpoints.up('sm')]: {
      display: 'flex',
      justifyContent: 'space-between'
    },
  },
  addActivityIcon: {
    fontSize: 50,
    color: Colors.green
  },
  ActivityCardButton: {
    width: '100%'
  },
  tag: {
    margin: 5
  },
});


class NewProject extends React.Component {

  constructor(props) {

    super(props);

    this.state = {
      activityToEdit: {},
      name: '',
      description: '',
      image: '',
      selectedFile: {},
      category: '',
      startDate: new Date(),
      address: '',
      latitude: '',
      longitude: '',
      showActivityModal: false,
      activities: {},
      error: {},
      loading: false
    }
  }

  completeness = 0;

  componentDidMount() {

    const { me } = this.props;

    if (!me.loggedIn) {
      this.props.history.push('/login');

      return
    }

    this.props.getCategories();

    Geocode.setApiKey(GoogleMapKey);
  }

  // On any state update we update the project level
  componentDidUpdate() {

    this.updateProjectCompletenessLevel();
  }


  handleInputChange = name => event => {

    this.setState({
      [name]: event.target.value
    }, this.checkFormErrors)
  }

  handleAddressChange = address => {

    this.setState({
      address
    }, this.checkFormErrors)
  }

  handleLocationChange = latLng => {

    this.setState({
      latitude: latLng.lat,
      longitude: latLng.lng
    })

    // Geocode.fromLatLng(latLng.lat, latLng.lng).then(
    //   response => {
    //     const address = response.results[0].formatted_address;
    //     console.log(address);
    //   },
    //   error => {
    //     console.error(error);
    //   }
    // )
  }


  checkFormErrors = () => {

    const { name, description, image, category, startDate, address, error } = this.state;

    const fields = [{name}, {description}, {image}, {category}, {startDate}, {address}];

    fields.forEach(field => {
      let key = Object.keys(field)[0],
        value = field[key];

      if (value.length === 0) {
        error[key] = 'This field is required'
      } else {
        delete error[key]
      }

    })

    this.setState({ error });

    return Object.keys(error).length === 0
  }


  openCloseActivityModal = value => () => {

    this.setState({
      showActivityModal: value
    });

    if (!value) {

      this.setState({
        activityToEdit: {}
      });
    }
  }

  addActivity = newAct => {

    let { activities } = this.state;

    let isNew = true;

    if (newAct.key !== undefined &&
      activities[newAct.key] !== undefined) {

      // This activity is already present, we are
      // updating it
      activities[newAct.key] = newAct;

      isNew = false;
    }

    if (isNew) {
      // New activity without unique key
      const newKey = this.generateUniqueKeyForActivity(newAct);

      newAct.key = newKey;

      activities[newKey] = newAct;
    }

    this.setState({
      showActivityModal: false,
      activityToEdit: {},
      activities
    })
  }

  generateUniqueKeyForActivity = act => {

    const { activities } = this.state;

    let newHash = getHash(act.name);

    if (activities[newHash] !== undefined) {
      newHash += Math.floor((Math.random() * 700) + 1)
    }

    return newHash;
  }

  activityClick = act => () => {

    this.setState({
      activityToEdit: act,
      showActivityModal: true
    })
  }


  updateProjectCompletenessLevel = () => {

    const { name, description, image, category,
      startDate, address, activities } = this.state;

    let score = 0;

    if (name.length)
      score += 20;

    if (description.length)
      score += 15;

    if (image.length)
      score += 15;

    if (category.length)
      score += 10;

    if (startDate !== '')
      score += 15;

    if (address.length)
      score += 10;

    // At this point 85 is the max score
    // Adding at least one activity does the 100
    if (Object.keys(activities).length)
      score += 15;


    score = Math.min(score, 100);

    this.completeness = score;
  }


  onChangePhoto = () => event => {

    const { files } = event.target,
      ValidTypes = ['image/jpeg', 'image/png', 'image/jpg', 'image/JPG', 'image/PNG', 'image/JPEG'];

    if (!files.length) {

      return;
    }

    if (ValidTypes.indexOf(files[0].type) === -1) {

      NotificationManager.error('Invalid File Type', 'Error!', 5000, () => { });

      return;
    }

    let reader = new FileReader();

    reader.onload = () => {

      this.setState({
        image: reader.result,
        selectedFile: files[0]
      }, this.checkFormErrors);
    };

    reader.readAsDataURL(files[0]);
  }


  onSaveChanges = () => {

    const { name, description, startDate, selectedFile,
      category, latitude, longitude, activities } = this.state;

    if (!this.checkFormErrors()) {

      return;
    }

    // Post new project
    this.setState({
      loading: true
    })

    const newProject = {
      name,
      description,
      start_date: startDate,
      latitude,
      longitude
    }

    this.props.createProject(newProject, selectedFile, [category], (res, id) => {

      this.setState({
        loading: false
      });

      if (res === 'error') {
        NotificationManager.error('Project upload failed', 'Try again', 3000, ()=> {});

      } else {

        this.props.addActivitiesToProject(id, Object.values(activities));

        NotificationManager.success('Project created!', 'Success', 3000, ()=> {});

        this.props.history.push('/home');
      }
    })
  }


  render() {

    const { classes, categoriesList } = this.props;

    const { error, activityToEdit, name, description, image, category, startDate,
      activities, showActivityModal, loading } = this.state;

    return (

      <div style={{paddingTop: 80}}>
        <Grid container justify="center">
          <Grid item xs={10} md={7}>
            <Divider style={{background: 'purple'}} />
            <SBView>
              <div className={classes.progressLeft}>
                <LText>NUEVO PROYECTO</LText>
                <GraySText>Suspendisse luctus ultrices lectus vel porttitor. In lectus diam, hendrerit a auctor ac, hendrerit a massa.</GraySText>
              </div>
              <CircularProgressbar
                percentage={this.completeness}
                text={`${this.completeness}%`}
                styles={{
                  root: {flex: 1, marginTop: 30},
                  path: { stroke: `rgba(248, 165, 30, ${0.2 + this.completeness / 125})` },
                  text: { fill: `rgba(20, 20, 20, ${0.2 + this.completeness / 100})`, fontSize: '24px', fontWeight: 'bold' },
                }}
              />
            </SBView>

            <TextField
              label="Nombre"
              placeholder="Nombre"
              className={classes.textField}
              value={name}
              onChange={this.handleInputChange('name')}
              margin="normal"
              variant="outlined"
              error={error.name ? true : false}
            />
            { error.name &&
              <ErrorText>{error.name}</ErrorText>
            }

            <TextField
              label="Descripción"
              placeholder="DEFINE TU PROYECTO"
              className={classes.textArea}
              value={description}
              onChange={this.handleInputChange('description')}
              margin="normal"
              variant="outlined"
              multiline
              rows={5}
              error={error.description ? true : false}
            />
            { error.description &&
              <ErrorText>{error.description}</ErrorText>
            }

            <Grid container justify="space-between">

              <Grid item xs={12} md={3}>
                <MText justify="center">Sube una imagen</MText>
                <center>
                  <Button className={classes.avatarView}>
                    <label>
                      <input type="file" ref={(ref) => this.fileEvent = ref} className={classes.fileInput} onChange={this.onChangePhoto()}/>

                      <IfView condition={image.length === 0}>

                        <AddIcon className={classes.avatarAddIcon}/>

                        <img alt="projectimage" src={image} className={classes.avatar} />
                      </IfView>
                    </label>
                  </Button>

                  { error.image &&
                    <ErrorText>{error.image}</ErrorText>
                  }
                </center>
              </Grid>

              <Grid item xs={12} md={8}>
                <div>
                  <MText>Categoría</MText>
                  <CustomInput
                    type="select"
                    label=""
                    value={category}
                    error={error.category ? true : false}
                    onChange={this.handleInputChange('category')}
                    options={categoriesList}
                  />
                  { error.category &&
                    <ErrorText>{error.category}</ErrorText>
                  }
                </div>

                <MText>Fecha</MText>
                <CustomDatePicker
                  date={startDate}
                  onSelect={(date) => this.setState({startDate: date})}
                />
                { error.startDate &&
                  <ErrorText>{error.startDate}</ErrorText>
                }

                <MText>Lugar</MText>
                <AddressPicker
                  selectCb={this.handleLocationChange}
                  changeCb={this.handleAddressChange}
                />
                { error.address &&
                  <ErrorText>{error.address}</ErrorText>
                }
              </Grid>

            </Grid>

            <Divider style={{marginTop: 50}} />
            <MText>Actividades</MText>
            <Grid container>
              {
                Object.keys(activities).map(key => {

                  let activity = activities[key];

                  return (
                    <Grid item xs={6} md={4} key={activity.key}>
                      <Button onClick={this.activityClick(activity)} className={classes.ActivityCardButton}>
                        <ActivityCard>
                          <Grid container>
                            <GraySText style={{height: 36, overflow: 'hidden', lineHeight: '18px', width: '100%'}}>{activity.name}</GraySText>
                            <LightGraySText style={{margin: 0}}>{activity.target}</LightGraySText>
                          </Grid>
                          <div className={classes.activityPriceView}>
                            <GraySText>{activity.type}</GraySText>
                            <LightGraySText>• {activity.price}€ / sesión</LightGraySText>
                          </div>
                        </ActivityCard>
                      </Button>
                    </Grid>
                  )
                })
              }

              <Grid item xs={6} md={4} key="new-activity">
                <Button onClick={this.openCloseActivityModal(true)} className={classes.ActivityCardButton}>
                  <AddActivityCard>
                    <AddIcon className={classes.addActivityIcon} />
                    <GraySText justify="center">AÑADIR ACTIVIDAD</GraySText>
                  </AddActivityCard>
                </Button>
              </Grid>
            </Grid>

            <Divider style={{marginTop: 50}}/>

            <RightView style={{padding: '40px 0 100px 0'}}>
              <OrangeButton loading={loading} text="Guardar cambios" paddingHorizontal={40} onPress={() => this.onSaveChanges()}/>
            </RightView>
          </Grid>
        </Grid>

        <NewActivity
          activity={activityToEdit}
          visible={showActivityModal}
          onCancel={this.openCloseActivityModal(false)}
          onSubmit={activityInfo => this.addActivity(activityInfo)}
        />

      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  me: state.userReducer,
  categoriesList: state.projectReducer.categoriesList
})

const mapDispatchToProps = ({
  createProject: projectActions.createProject,
  getCategories: projectActions.getCategories,
  addActivitiesToProject: projectActions.addActivitiesToProject
})

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(NewProject));
