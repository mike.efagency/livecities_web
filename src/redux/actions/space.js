import Axios from 'axios';
import { NotificationManager } from 'react-notifications';
import * as types from '../types';
import { Space__exampleData } from '../../lib/constants';
import { apply } from '../../lib/operators';
/************/



// AUX FUNCTIONS

/**
 * Used to set the Authorization header field with current Token
 *
 * @return {void}
 */
 export const setAuthToken = () => (dispatch, getState) => {

   const state = getState(),
     token = state.userReducer.token;

   if (token) {

     Axios.defaults.headers.common['Authorization'] = token;

   } else {
     Axios.defaults.headers.common['Authorization'] = null;
     delete Axios.defaults.headers.common['Authorization'];
   }
 }

/************/



// MAIN FUNCTIONS

/**
 * Retrieve the space information from backend.
 * SET_SPACE action is dispatched with the space details as payload.
 *
 * @param {int} id - of the resource to fetch
 *
 * @return {void}
 */
export const getSpace = (id) => dispatch => {

  dispatch(setAuthToken());

  Axios.get(`${types.URL_GET_SPACE}/${id || 6}`)
  .then(res => {

    dispatch({
      type: types.SET_SPACE,
      payload: res.data
    });

  })
	.catch(() => {

    const data = Space__exampleData;

    dispatch({
      type: types.SET_SPACE,
      payload: data
    });
	})

}

/**
 * Post message to resource owner with a date to dispose of the space.
 *
 * @param  {object}   params with the form details
 * @param  {function} callback to be executed after endpoint gets resolved
 * @return {void}
 */
export const sendMessageToOwner = (params, callback) => (dispatch) => {

  dispatch(setAuthToken());

  // TODO: change this URL
  Axios.get(types.URL_GET_ALL_TASKS)
  .then(() => {

    apply(callback);

  })
  .catch(e => {
    console.log(e.toString());
  })
};

/**
 * Used to PUT a new resource along with its images
 *
 * @param  {object}   params   project details
 * @param  {array}   images    of image objects
 * @param  {function} callback to be executed after the endpoint gets resolved
 *
 * @return {void}
 */
export const createSpace = (params, images, callback) => dispatch => {

  dispatch(setAuthToken());

  Axios.put(types.URL_CREATE_RESOURCES, params)
  .then(res => {

    if (res.status === 200) {

      NotificationManager.success('Resource has been created.', 'Success');

      // Post images
      dispatch(uploadResourceImages(res.data.id, images));

      apply(callback('success'));

    } else {

      NotificationManager.error('Error occured', 'Failed', 3000, () => {});

      callback('error');
    }
  })
  .catch(e => {

    NotificationManager.error('Error occured', 'Failed', 3000, () => {console.log(e)});

    callback('error');
  })
}

/**
 * Post image to project.
 * SET_PROJECT_IMAGE action is dispatched after the endpoint gets resolved.
 *
 * @param  {int} id - unique id of the project
 * @param  {array} images - array of files to be uploaded
 *
 * @return {void}
 */
export const uploadResourceImages = (id, images) => dispatch => {

  images.forEach(image => {

    let formData = new FormData();

    formData.append('image', image.selectedFile);

    dispatch(setAuthToken());

    Axios.post(`${types.URL_SET_IMAGE_TO_PROJECT}/${id}/image`, formData)
    .then()
    .catch(e => {
      console.log(e.toString())
    })

  })
}
