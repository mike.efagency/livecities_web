import Axios from 'axios';
import * as types from '../types';
import { apply } from '../../lib/operators';
/************/



// SIMPLE ACTIONS

export const setSearchText = text => ({
  type: types.SET_SEARCH_TEXT,
  payload: text
})

/************/



// MAIN FUNCTIONS

// TODO: check the use case of this function
export const getSearchResultByClass = className => dispatch => {

  dispatch({
    type: types.SET_SEARCH_LOADING,
    payload: true
  });

  if (className === 'resource') {

    dispatch(getAllResources());

  } else if (className === 'project') {

    dispatch(getAllTasks());

  } else {
    dispatch(getAllResources());
    dispatch(getAllTasks());
  }
}

/**
 * Used to retrieve all resources to show in the search page. GET call to search
 * endpoint.
 * SET_RESOURCES action is dispatched with the resources array as payload.
 *
 * @return {void}
 */
export const getAllResources = () => dispatch => {

  Axios.get(types.URL_GET_ALL_RESOURCES)
  .then(res => {

    // Clearing resources
    dispatch({
      type: types.SET_RESOURCES,
      payload: []
    });

    const { items } = res.data;

    if (res.status === 200 && items && items.length) {

      let resources = items.filter(item => {

        return item.model_class === 'Resource';
      });

      dispatch({
        type: types.SET_RESOURCES,
        payload: resources
      });
    }

    dispatch({
      type: types.SET_SEARCH_LOADING,
      payload: false
    })

  })
  .catch(e => {

    console.log(e.toString());

    dispatch({
      type: types.SET_SEARCH_LOADING,
      payload: false
    })
  })
}

/**
 * Used to retrieve all projects to show in the search page. GET call to search
 * endpoint.
 * SET_PROJECTS action is dispatched with the tasks array as payload.
 *
 * @return {void}
 */
export const getAllTasks = () => dispatch => {

  Axios.get(types.URL_GET_ALL_TASKS)
  .then(res => {

    // Clearing projects
    dispatch({
      type: types.SET_PROJECTS,
      payload: []
    });

    const { items } = res.data;

    if (res.status === 200 && items && items.length){

      let tasks = items.filter(task => {

        return task.model_class === 'Task';
      });

      dispatch({
        type: types.SET_PROJECTS,
        payload: tasks
      });
    }

    dispatch({
      type: types.SET_SEARCH_LOADING,
      payload: false
    })

  })
  .catch(e => {
    console.log(e.toString());

    dispatch({
      type: types.SET_SEARCH_LOADING,
      payload: false
    })
  })

}

/**
 * Used to retrieve projects and resources based on a search string.
 * SET_PROJECTS and SET_RESOURCES actions are dispatched with tasks and
 * resources arrays as payload.
 *
 * @param {string} searchText to be used in the query
 *
 * @return {void}
 */
export const getSearchResultByText = searchText => dispatch => {

  dispatch({
    type: types.SET_SEARCH_LOADING,
    payload: true
  })

  Axios.get(`${types.URL_GET_SEARCH_RESULT_BY_TEXT}${searchText}`)
  .then(res => {

    // Clear projects
    dispatch({
      type: types.SET_PROJECTS,
      payload: []
    });

    // Clear resources
    dispatch({
      type: types.SET_RESOURCES,
      payload: []
    });

    const { items } = res.data;

    if (res.status === 200 && items && items.length) {

      let resources = [],
        projects = [];

      items.forEach(item => {

        if (item.model_class === 'Resource') {
          resources.push(item);

        } else if (item.model_class === 'Task') {
          projects.push(item);
        }
      })

      dispatch({
        type: types.SET_RESOURCES,
        payload: resources
      });

      dispatch({
        type: types.SET_PROJECTS,
        payload: projects
      });
    }

    dispatch({
      type: types.SET_SEARCH_LOADING,
      payload: false
    })
  })
  .catch(e => {

    console.log(e.toString());

    dispatch({
      type: types.SET_SEARCH_LOADING,
      payload: false
    })
  })
}

/**
 * Search call used to get more items from the current search. It does use the
 * offset param of this endpoint.
 * ADD_MORE_TASKS / ADD_MORE_PROJECTS action is dispatched once the call gets resolved.
 *
 * @param  {string}   type of item to be searched
 * @param  {function} callback to be executed afthe the call gets resolved
 *
 * @return {void}
 */
export const loadMoreByClass = (type, callback) => dispatch => {

  // TODO: add the offset param to the call
  // ADD also other search call params

  Axios.get(`${types.URL_GET_SEARCH_RESULT}${type}`)
  .then(res => {

    const { items } = res.data;

    let reducerToUse = type === 'task'
      ? types.ADD_MORE_PROJECTS
      : types.ADD_MORE_RESOURCES;

    if (res.status === 200 && items && items.length) {

      dispatch({
        type: reducerToUse,
        payload: items
      });
    }

    apply(callback('success'));

  })
  .catch(e => {
    console.log(e.toString());
  })

}
