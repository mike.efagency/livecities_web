import * as authActions from './auth';
import * as screenActions from './screen';
import * as homeActions from './home';
import * as searchActions from './search';
import * as projectActions from './project';
import * as spaceActions from './space';
import * as activityActions from './activities';

export {
  authActions,
  screenActions,
  homeActions,
  searchActions,
  projectActions,
  spaceActions,
  activityActions
};