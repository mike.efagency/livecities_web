import Axios from 'axios';
import * as types from '../types';
import { Activities__exampleData } from '../../lib/constants';

/************/





/**
 * Retrieve user activities.
 * SET_USER_ACTIVITIES action is dispatched with the activities array as payload.
 *
 * @return {void}
 */
export const getUserActivities = () => dispatch => {

  // TODO: change this URL and remove the example data
  Axios.get(types.URL_GET_ALL_TASKS)
  .then(() => {

    dispatch({
      type: types.SET_USER_ACTIVITIES,
      payload: Activities__exampleData
    });

  })
  .catch((e) => {
    console.log(e.toString());
  })
}

/**
 * Post new user activity
 * ADD_NEW_ACTIVITY action is dispatched with the activity as payload.
 *
 * @param  {object} params of the new activity
 * @param  {function} callback to be executed after the call gets resolved
 *
 * @return {void}
 */
export const createActivity = (params, callback) => dispatch => {

  // TODO: change this URL
  Axios.get(types.URL_GET_ALL_TASKS)
  .then(() => {

    dispatch({
      type: types.ADD_NEW_ACTIVITY,
      payload: params
    });

    callback('success');

  })
  .catch((e) => {
    console.log(e.toString());

    callback('error');
  })
};

/**
 * Edit an existing user activity
 *
 * @param  {object} params of the activity
 * @param  {function} callback to be executed after the call gets resolved
 *
 * @return {void}
 */
export const editActivity = (params, callback) => () => {

  // TODO: change this URL
  Axios.get(types.URL_GET_ALL_TASKS)
  .then(() => {

    callback('success');

  })
  .catch((e) => {
    console.log(e.toString());

    callback('error');
  })
};
