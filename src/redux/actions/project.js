import Axios from 'axios';
import { NotificationManager } from 'react-notifications';
import * as types from '../types';
import { apply } from '../../lib/operators';
/************/



// AUX FUNCTIONS

/**
 * Used to set the Authorization header field with current Token
 *
 * @return {void}
 */
export const setAuthToken = () => (dispatch, getState) => {

  const state = getState(),
    token = state.userReducer.token;

  if (token) {

    Axios.defaults.headers.common['Authorization'] = token;

  } else {
    Axios.defaults.headers.common['Authorization'] = null;
    delete Axios.defaults.headers.common['Authorization'];
  }
}

/************/



// MAIN FUNCTIONS

/**
 * Used to PUT a new project (task) along with its image and tags
 *
 * @param  {object}   params   project details
 * @param  {object}   image    image object
 * @param  {array}   tagArray  tags (category) of the project
 * @param  {function} callback to be executed after the endpoint gets resolved
 *
 * @return {void}
 */
export const createProject = (params, image, tagArray, callback) => dispatch => {

  dispatch(setAuthToken());

  Axios.put(types.URL_CREATE_PROJECT, params)
  .then(res => {

    if (res.status === 200) {

      NotificationManager.success('Profile has been created.', 'Success');

      dispatch({
        type: types.CREATE_PROJECT,
        payload: params
      });

      // Post image
      dispatch(uploadProjectImage(res.data.id, image));

      // Post tags
      dispatch(addTagsToProject(res.data.id, tagArray));

      apply(callback('success'));

    } else {

      NotificationManager.error('Error occured', 'Failed', 3000, () => {});

      callback('error');
    }
  })
  .catch(e => {

    NotificationManager.error('Error occured', 'Failed', 3000, () => {console.log(e)});

    callback('error');
  })
}

/**
 * Post image to project.
 * SET_PROJECT_IMAGE action is dispatched after the endpoint gets resolved.
 *
 * @param  {int} id       unique id of the project
 * @param  {object} image file to be uploaded
 *
 * @return {void}
 */
export const uploadProjectImage = (id, image) => dispatch => {

  let formData = new FormData();

  formData.append('image', image);

  dispatch(setAuthToken());

  Axios.post(`${types.URL_SET_IMAGE_TO_PROJECT}/${id}/image`, formData)
  .then(res => {

    dispatch({
      type: types.SET_PROJECT_IMAGE,
      payload: res.data.url
    });

  })
  .catch(e => {
    console.log(e.toString())
  })
}

/**
 * Post tags to project.
 * SET_PROJECT_TAGS action is dispatched after the endpoint gets resolved.
 *
 * @param {int} id         unique id of the project
 * @param {array} tagArray to be uploaded
 *
 * @return {void}
 */
export const addTagsToProject = (id, tagArray) => dispatch => {

  dispatch(setAuthToken());

  tagArray.forEach(tag => {

    Axios.put(`${types.URL_ADD_TAG_TO_PROJECT}/${id}/tags`, tag)
    .then(res => {

      console.log(res, 'tag has been added');
    });
  });

  dispatch({
    type: types.SET_PROJECT_TAGS,
    payload: tagArray
  });

}

/**
 * Post activities to project.
 *
 * @param {int} id - unique id of the project
 * @param {array} activities - to be uploaded
 *
 * @return {void}
 */
export const addActivitiesToProject = (id, activities) => dispatch => {

  dispatch(setAuthToken());

  activities.forEach(act => {

    // Axios.put(`${types.URL_ADD_TAG_TO_PROJECT}/${id}/activity`, act)
    // @TODO: change this URL when backend is ready
    Axios.get(types.URL_GET_PROJECT_CATEGORIES)
    .then(res => {

      console.log(res, act, 'Activity has been posted');
    });
  });

}

/**
 * Get all available categories to be choosen for a project.
 * SET_PROJECT_CATEGORIES action is dispatched after the endpoint gets resolved.
 *
 * @return {void}
 */
export const getCategories = () => dispatch => {

  dispatch(setAuthToken());

  Axios.get(types.URL_GET_PROJECT_CATEGORIES)
  .then(res => {

    if (res.data.length > 0) {

      let categoryList = JSON.parse(res.data).map(category => {
        return category.name;
      });

      dispatch({
        type: types.SET_PROJECT_CATEGORIES,
        payload: categoryList
      })
    }
  })

}
