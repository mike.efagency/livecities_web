import Axios from 'axios';
import * as types from '../types';

import { shuffle } from '../../lib/operators';
/************/



// MAIN FUNCTIONS

/**
 * Used to retrieve the resources to show in the home page. GET call to search
 * endpoint.
 * SET_RESOURCES action is dispatched with the resources array as payload.
 *
 * @return {void}
 */
export const getHomeResources = () => dispatch => {

  Axios.get(types.URL_HOME_GET_RESOURCES)
  .then(res => {

    // Clearing resources
    dispatch({
      type: types.SET_RESOURCES,
      payload: []
    });

    const { items } = res.data;

    if (res.status === 200 && items && items.length) {

      let resources = items.filter(item => {

        return item.model_class === 'Resource';
      });

      resources = shuffle(resources).slice(0, 4);

      resources.forEach(res => {
        res.ratingsCount = Math.floor(Math.random() * 51);
        res.rating = Math.floor(Math.random() * 6);
      })

      dispatch({
        type: types.SET_RESOURCES,
        payload: resources
      });
    }

  })
  .catch(e => {
    console.log(e.toString());
  })

}

/**
 * Used to retrieve the projects (tasks) to show in the home page. GET call to search
 * endpoint.
 * SET_PROJECTS action is dispatched with the tasks array as payload.
 *
 * @return {void}
 */
export const getHomeProjects = () => dispatch => {

  Axios.get(types.URL_HOME_GET_TASKS)
  .then(res => {

    // Clearing projects
    dispatch({
      type: types.SET_PROJECTS,
      payload: []
    });

    const { items } = res.data;

    if (res.status === 200 && items && items.length) {

      let tasks = items.filter(task => {

        return task.model_class === 'Task';
      });

      dispatch({
        type: types.SET_PROJECTS,
        payload: tasks
      });
    }
  })
  .catch(e => {
    console.log(e.toString());
  })

}

/**
 * Get total resources present in the DB using the search api call and filtering by class.
 * SET_TOTAL_RESOURCES action is dispatched with the resources count as payload.
 *
 * @return {void}
 */
export const getHomeTotalResources = () => dispatch => {

  Axios.get(types.URL_HOME_GET_TOTAL_RESOURCES)
  .then(res => {

    const { total } = res.data;

    let totalResources = 0;

    if (res.status === 200 && total) {
      totalResources = total;
    }

    dispatch({
      type: types.SET_TOTAL_RESOURCES,
      payload: totalResources
    });

  })
  .catch(e => {
    console.log(e.toString());
  })

}
