import Axios from 'axios';
import * as types from '../types';
import { UserInformation__exampleData } from '../../lib/constants';
import { apply } from '../../lib/operators';

/************/



// SIMPLE ACTIONS

export const logOut = () => ({
  type: types.LOGOUT,
});

/************/



// AUX FUNCTIONS

/**
 * Used to set the Authorization header field with current Token
 *
 * @return {void}
 */
export const setAuthToken = () => (dispatch, getState) => {

  const state = getState(),
    token = state.userReducer.token;

  if (token) {

    Axios.defaults.headers.common['Authorization'] = token;

  } else {
    Axios.defaults.headers.common['Authorization'] = null;
    delete Axios.defaults.headers.common['Authorization'];
  }
}

/************/



// MAIN FUNCTIONS

/**
 * Used to create a new user account.
 * SET_TOKEN action is dispatched with the session token as payload.
 *
 * @param  {object}   params with the new account details
 * @param  {function} callback to be executed after the call gets resolved
 *
 * @return {void}
 */
export const signup = (params, callback) => dispatch => {

  Axios.put(types.URL_SIGNUP, params)
  .then(res => {

    if (res.data.token) {

      dispatch({
        type: types.SET_TOKEN,
        payload: res.data.token
      });

      apply(
        callback(true)
      );

    } else {

      callback(false, 'Could not create the new account.');
    }
  })
  .catch(error => {

    let msg = 'Could not create the new account.';

    if (error.response && error.response.data &&
      Array.isArray(error.response.data.email) && error.response.data.email.length) {

      msg = error.response.data.email[0]
    }

    callback('error', msg);
  })
};

/**
 * Post to /login to retrieve a session token and grant access to protected api calls.
 * SET_TOKEN action is dispatched with the session token as payload.
 *
 * @param  {object}   params with the login details
 * @param  {function} callback to be executed after the call gets resolved
 *
 * @return {void}
 */
export const login = (params, callback) => dispatch => {

  Axios.post(types.URL_LOGIN, params)
  .then(res => {

    dispatch({
      type: types.SET_TOKEN,
      payload: res.data.token
    });

    apply(
      callback(true)
    );
  })
  .catch(e => {

    const status = e.response.status;

    if (status === 400) {
      callback(false, 'A user with this email and password was not found.');
    } else {
      callback(false, 'Unknown Error');
    }
  })
};

/**
 * Post user avatar.
 * SET_USER_AVATAR action is dispatched with the image url as payload.
 *
 * @param  {object}   image blob to be uploaded
 * @param  {function} callback to be executed after the call gets resolved
 *
 * @return {void}
 */
export const uploadImage = (image, callback) => dispatch => {

  let formData = new FormData();

  formData.append('image', image);

  dispatch(setAuthToken());

  Axios.post(types.URL_SET_AVATAR, formData)
  .then(res => {

    if (res.data.url !== undefined) {

      dispatch({
        type: types.SET_USER_AVATAR,
        payload: res.data.url
      });

      apply(
        callback('success', '')
      );
    }
  })
  .catch((e) => {

    console.log(e.toString())

    callback('error', e.toString());
  })

}

/**
 * Put skill tag for the current user
 * SET_USER_SKILLS action is dispatched with the skill as payload.
 *
 * @param  {string} tag to add
 * @param  {function} callback to be executed after the endpoint gets resolved
 *
 * @return {void}
 */
export const putSkill = (tag, callback = () => {}) => dispatch => {

  dispatch(setAuthToken());

  Axios.put(types.URL_USER_SET_SKILLS, { tag })
  .then(() => {

    dispatch({
      type: types.SET_USER_SKILLS,
      payload: tag
    });

    apply(
      callback(true)
    );

  })
  .catch((e) => {

    console.log(e.toString())

    callback(false);
  })

}

/**
 * Used to retrieve the user information.
 * SET_USER action is dispatched with the user object as payload.
 *
 * @return {void}
 */
export const getProfile = () => dispatch => {

  dispatch(setAuthToken());

  Axios.get(types.URL_GET_PROFILE)
  .then(res => {

    // TODO: remove this UserInformation__exampleData hardcoded data
    // Object.keys(UserInformation__exampleData).forEach(key => {
    //   res.data[key] = UserInformation__exampleData[key]
    // })

    dispatch({
      type: types.SET_USER,
      payload: {...res.data, ...UserInformation__exampleData}
    })

  })
  .catch(e => {
    console.log(e.toString());
  })

}

/**
 * Get available skills to show in the sign up page. This endpoint will be called as autocomplete.
 *
 * @param  {string} searchStr to use in the query
 * @param  {function} callback to be executed after the call gets resolved
 *
 * @return {void}
 */
export const getSkills = (searchStr, callback) => () => {

  Axios.get(`${types.URL_SKILLS_AUTOCOMPLETE}${searchStr}?isa=knowledge&size=9`)
  .then(res => {

    apply(callback(res.data.results.items));

  })
  .catch(e => {

    apply(callback([]));

    console.log(e.toString())
  })

}

/**
 * Used to update some info from the user. Name and email. Also
 * schedule, distance, courses and education.
 * SET_USER action is dispatched with the user object as payload.
 *
 * @param  {[type]} newUserInfo [description]
 *
 * @return {void}
 */
export const updateProfile = newUserInfo => dispatch => {

  // TODO: change the url if the API finally accepts this
  Axios.get(types.URL_GET_ALL_TASKS)
  .then(() => {

    console.log(newUserInfo);

    dispatch({
      type: types.SET_USER,
      payload: newUserInfo
    });

  })
  .catch((e) => {
    console.log(e.toString())
  })

};

/************/
