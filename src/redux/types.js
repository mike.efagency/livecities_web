
// API
export const baseURL = 'https://livecities-back.8wires.io/v0';

export const URL_LOGIN = `${baseURL}/login`;
export const URL_SIGNUP = `${baseURL}/users`;

export const URL_GET_PROFILE = `${baseURL}/me`;
export const URL_SET_AVATAR =  `${baseURL}/me/image`;
export const URL_USER_SET_SKILLS = `${baseURL}/me/tags`;

export const URL_SKILLS_AUTOCOMPLETE = `${baseURL}/resources/a/`;

export const URL_HOME_GET_RESOURCES = `${baseURL}/search?class=resource&size=4`;
export const URL_HOME_GET_TASKS = `${baseURL}/search?class=task&size=9`;
export const URL_HOME_GET_TOTAL_RESOURCES = `${baseURL}/search?class=resource&kind=not-k`;

export const URL_GET_RESOURCES = `${baseURL}/resources`;
export const URL_GET_PROJECTS = `${baseURL}/project`;
export const URL_GET_ALL_RESOURCES = `${baseURL}/search?class=resource`;

export const URL_GET_ALL_TASKS = `${baseURL}/search?class=task`;

export const URL_GET_SEARCH_RESULT_BY_TEXT = `${baseURL}/search?q=`;
export const URL_GET_SKILL_TAGS = `${baseURL}/search?class=resourceclass&isa=knowledge`;
export const URL_GET_PROJECT_CATEGORIES = `${baseURL}/tags/all`;
export const URL_GET_SEARCH_RESULT = `${baseURL}/search?class=`;

export const URL_CREATE_PROJECT = `${baseURL}/tasks/`;
export const URL_ADD_TAG_TO_PROJECT = `${baseURL}/tasks/`; // + id/tags
export const URL_SET_IMAGE_TO_PROJECT = `${baseURL}/tasks/`; // + id/image

export const URL_CREATE_RESOURCES = `${baseURL}/resources`;

export const URL_UPDATE_PROFILE = baseURL + '/resources/5/follow';  // TODO: @roger - change this endpoint when ready
export const URL_UPDATE_PROFILE_INFO = baseURL + '/resources/5/follow';  // TODO: @roger - change this endpoint when ready
export const URL_GET_PROFILE_INFO = baseURL + '/resources/6' // TODO: @roger - change this endpoint when ready
export const URL_CREATE_ACTIVITY = baseURL + '/resources/5/follow';  // TODO: @roger - change this endpoint when ready
export const URL_GET_ACTIVITIES = baseURL + '/resources/6' // TODO: @roger - change this endpoint when ready
export const URL_GET_SPACE = baseURL + '/resources' // remember to add here the ID of the space to retrieve
export const URL_ASK_SPACE = baseURL + '/resources/5/follow';  // TODO: @roger - change this endpoint when ready


// Responsive Action
export const SET_DIMENSION = 'SET_DIMENSION';


// Authentication
export const SET_TOKEN = 'SET_TOKEN';
export const LOGOUT = 'LOGOUT';


// User
export const SIGN_UP = 'SIGN_UP';

export const SET_USER = 'SET_USER';
export const SET_USER_AVATAR = 'SET_USER_AVATAR';
export const SET_USER_SKILLS = 'SET_USER_SKILLS';
export const SET_USER_ACTIVITIES = 'SET_USER_ACTIVITIES';
export const ADD_NEW_ACTIVITY = 'ADD_NEW_ACTIVITY';


// Home page
export const SET_RESOURCES = 'SET_RESOURCES';
export const SET_PROJECTS = 'SET_PROJECTS';
export const SET_TOTAL_RESOURCES = 'SET_TOTAL_RESOURCES';

// Search page
export const SET_SEARCH_TEXT = 'SET_SEARCH_TEXT';
export const SET_SEARCH_LOADING = 'SET_SEARCH_LOADING';
export const ADD_MORE_PROJECTS = 'ADD_MORE_PROJECTS';
export const ADD_MORE_RESOURCES = 'ADD_MORE_RESOURCES';

// Project
export const CREATE_PROJECT = 'CREATE_PROJECT';
export const RESET_PROJECT = 'RESET_PROJECT';
export const SET_PROJECT_CATEGORIES = 'SET_PROJECT_CATEGORIES';
export const SET_PROJECT_IMAGE = 'SET_PROJECT_IMAGE';
export const SET_PROJECT_TAGS = 'SET_PROJECT_TAGS';

// Space
export const SET_SPACE = 'SET_SPACE';
export const SET_SPACE_MESSAGE = 'SET_SPACE_MESSAGE';
export const RESET_SPACE_RESOURCE = 'RESET_SPACE_RESOURCE';
