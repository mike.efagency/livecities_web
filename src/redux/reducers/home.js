import * as types from '../types';

const INITIAL_STATE = {
  resources: [],
  projects: [],
  totalResources: 0,
  searchText: '',
  searching: false
};

const homeReducer = (state = INITIAL_STATE, action) => {
  switch(action.type) {

    case types.SET_RESOURCES:
      return {
        ...state,
        resources: action.payload
      }

    case types.SET_PROJECTS:
      return {
        ...state,
        projects: action.payload
      }

    case types.SET_TOTAL_RESOURCES:
      return {
        ...state,
        totalResources: action.payload
      }

    case types.ADD_MORE_RESOURCES:
      return {
        ...state,
        resources: [...state.resources, action.payload]
      }

    case types.ADD_MORE_PROJECTS:
      return {
        ...state,
        projects: [...state.projects, action.payload]
      }

    case types.SET_SEARCH_TEXT:
      return {
        ...state,
        searchText: action.payload
      }

    case types.SET_SEARCH_LOADING:
      return {
        ...state,
        searching: action.payload
      }

    default:
      return state;
  }
}

export default homeReducer;
