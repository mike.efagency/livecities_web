import * as types from '../types';

const INITIAL_STATE = {
  name: '',
  description: '',
  imageURL: '',
  category: 'Select Category',
  startDate: '',
  endDate: '',
  tags: [],
  activities: [],
  categoriesList: [],
};

const projectReducer = (state = INITIAL_STATE, action) => {

  switch(action.type) {

    case types.CREATE_PROJECT:
      return {
        ...state,
        ...action.payload
      }

    case types.SET_PROJECT_IMAGE:
      return {
        ...state,
        imageURL: action.payload
      }

    case types.SET_PROJECT_TAGS:
      return {
        ...state,
        tags: action.payload
      }

    case types.SET_PROJECT_CATEGORIES:
      return {
        ...state,
        categoriesList: action.payload
      }

    case types.RESET_PROJECT:
      return INITIAL_STATE;

    default:
      return state;
  }
}

export default projectReducer;
