import * as types from '../types';
import { defaultAvatar } from '../../lib/images';

const INITIAL_STATE = {
  user: {
    id: 1,
    image: defaultAvatar,
    name: '',
    username: '',
    email: '',
    tags: [],
    knowledge: [],
    // Props to be added in backend
    distance: 0,
    schedule: '',
    education: [],
    courses: [],
  },
  activities: [],
  token: '',
  loggedIn: false,
};

const userReducer = (state = INITIAL_STATE, action) => {

  switch(action.type) {

    case types.SET_USER:
      return {
        ...state,
        user: action.payload
      }

    case types.SET_USER_AVATAR:
      return {
        ...state,
        user: {
          ...state.user,
          image: action.payload
        }
      }

    case types.SET_USER_ACTIVITIES:
      return {
        ...state,
        activities: action.payload
      }

    case types.ADD_NEW_ACTIVITY:
      return {
        ...state,
        activities: [...state.activities, action.payload]
      }

    case types.SET_USER_SKILLS:
      return {
        ...state,
        user: {
          ...state.user,
          tags: action.payload
        }
      }

    case types.SET_TOKEN:
      return {
        ...state,
        token: action.payload,
        loggedIn: true
      }

    case types.LOGOUT:
      return INITIAL_STATE

    default:
      return state;
  }
}

export default userReducer;
