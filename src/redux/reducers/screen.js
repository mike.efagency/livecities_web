import * as types from '../types';

const INITIAL_STATE = {
  width: 0,
  height: 0
};

const screenReducer = (state = INITIAL_STATE, action) => {

  switch(action.type) {

    case types.SET_DIMENSION:
      return {
        ...state,
        width: action.payload.width,
        height: action.payload.height
      }

    default: 
      return state;
  }
}

export default screenReducer;
