import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import userReducer from './user';
import screenReducer from './screen';
import homeReducer from './home';
import projectReducer from './project';
import spaceReducer from './space';

const rootReducer = combineReducers({
  router: routerReducer,
  userReducer,
  homeReducer,
  screenReducer,
  projectReducer,
  spaceReducer
});

export default rootReducer;
