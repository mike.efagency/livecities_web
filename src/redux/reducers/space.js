
import * as types from '../types';

const INITIAL_STATE = {
  selectedResource: {
    id: 0,
    name: '',
    description: '',
    isa: null,
    is_available: true,
    kind: 'U',
    latitude: 0,
    longitude: 0,
    owner: null,
    owner_name: '',
    average_rating: 0,
    blocked_dates: [],
    ratings_distribution: [
      {rating: 1, count: 0, percent: 0},
      {rating: 2, count: 0, percent: 0},
      {rating: 3, count: 0, percent: 0},
      {rating: 4, count: 0, percent: 0},
      {rating: 5, count: 0, percent: 0}
    ],
    created: '',
    team: null
  },
  message: {

  }
};

const spaceReducer = (state = INITIAL_STATE, action) => {

  switch(action.type) {

    case types.SET_SPACE:
      return {
        ...state,
        selectedResource: {
          ...state.selectedResource,
          ...action.payload,
        }
      }

    case types.SET_SPACE_MESSAGE:
      return {
        ...state,
        message: action.payload
      }

    case types.RESET_SPACE_RESOURCE:
      return INITIAL_STATE;

    default:
      return state;
  }
}

export default spaceReducer;
