import styled from 'styled-components';

export const MapWrapper = styled.div`
  padding-bottom: 40px;
  height: 90%;
  min-height: 300px;
`