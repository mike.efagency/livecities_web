import styled from 'styled-components';
import { welcomeBackground } from '../lib/images';

export const TopWrapper = styled.div`
  flex: 1;
  height: 300px;
  width: 100%;
  background-image: url(${welcomeBackground});
  background-size: cover;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin-bottom: 30px;
`
export const W60 = styled.div`
  width: 60%;
`
export const AvatarSection = styled.div`
  margin-top: 30px;
  border-top: 1px solid lightgray;
  border-bottom: 1px solid lightgray;
  padding: 20px 0;
`