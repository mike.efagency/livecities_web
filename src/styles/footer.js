import styled from 'styled-components';
import { Colors } from '../lib/theme';

export const Container = styled.div`
  background-color: ${Colors.blue}
`

export const StoreIcon = styled.img`
  width: 150px;
  height: 50px;
  margin-bottom: 10px;
`

export const BottomLogo = styled.span`
  padding: 50px 0px;
  font-size: 12px;
  color: white;
`