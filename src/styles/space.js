import styled from 'styled-components';
import { background9 } from '../lib/images';

export const ImageView = styled.div`
  width: 100%;
  height: 40vh;
  min-height: 300px;
  background-image: url(${background9});
  background-size: cover;
  padding: 20px;
  display: flex;
  flex-direction: column;
  justify-content: space-between
`

export const EquipmentView = styled.div`
  display: flex;
  flex-wrap: wrap;
  padding: 10px 0 30px 0;
`

export const EquipmentTag = styled.span`
  height: 30px;
  border-radius: 15px;
  padding: 0 20px;
  border: 1px solid gray;
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 5px 10px;
  font-size: 12px;
`

export const TableView = styled.div`
  border-radius: 10px;
  border: 1px solid rgba(0, 0, 0, 0.12);
`

export const TableIcon = styled.img`
  width: 30px;
  height: 30px;
  object-fit: contain;
  margin-right: 20px;
`

export const Avatar = styled.img`
  width: 50px;
  height: 50px;
  object-fit: cover;
  margin-right: 20px;
  border-radius: 25px;
`

export const ApplyContent = styled.div`
  background-color: rgba(61, 37, 82, 0.9);
  padding: 0 20px;
  width: ${props => props.width + 'px'};
  position: relative;
  z-index: 10;
  border-radius: 8px;
`
