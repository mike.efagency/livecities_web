import styled from 'styled-components';
import { background9 } from '../lib/images';

export const AvatarSection = styled.div`
  background-image: url(${background9});
  background-size: cover;
  padding: 30px;
`
export const ActivityCard = styled.div`
  padding: 10px;
  border-radius: 4px;
  border: 1px solid lightgray;
  height: 160px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  width: 100%;
`
export const AddActivityCard = styled.div`
  padding: 10px;
  border-radius: 4px;
  border: 1px solid lightgray;
  height: 160px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100%;
`
export const NewActivityWrapper = styled.div`
  position: fixed;
  top: 0;
  right: 0;
  left: 0;
  bottom: 0px;
  overflow-y: scroll;
  background-color: rgba(30, 10, 60, 0.9);
  z-index: 10000;
  @media (max-width: 700px) {
    padding: 50px 0;
  }
  @media (min-width: 700px) {
    padding: 100px 0;
  }
`
