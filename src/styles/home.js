import styled from 'styled-components';
import { background1, background9 } from '../lib/images';

export const TopWrapper = styled.div`
  background-image: url(${background1});
  height: 600px;
  display: flex;
  flex-direction: column;
`
export const SearchWrapper = styled.div`
  width: 100%;
  padding: 30px 0px;
  background-color: rgba(0, 0, 0, 0.5);
`

export const BottomWrapper = styled.div`
  background-image: url(${background9});
  background-size: cover;
  margin-top: 80px;
`
export const BottomBackground = styled.div`
  padding: 50px;
  display: flex;
  flex-direction: column;
  align-items: center;
  background: linear-gradient(to right, rgba(30, 30, 160, 0.5), rgba(250, 60, 40, 0.5));
`
export const CardWrapper = styled.div`
  width: 100%;
  text-transform: none
`
export const CardImage = styled.div`
  width: 100%;
  height: 200px;
  background-image: url(${props => props.src});
  background-size: cover
`
export const Dot = styled.div`
  width: 10px;
  height: 10px;
  border-radius: 10px;
  margin-right: 10px;
  background-color: ${props => props.bgColor}
`
export const AuthorName = styled.span`
  color: blue
`