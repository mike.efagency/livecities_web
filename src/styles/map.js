import styled from 'styled-components';
import { Colors } from '../lib/theme';

export const MarkerWrapper = styled.div`
  border-radius: 4px;
  border: 1px solid gray;
  padding: 10px 20px;
  font-size: 20px;
  background-color: ${Colors.green};
  color: white;
  margin-top: -30px
`