import styled from 'styled-components';

export const LogoImage = styled.img`
  width: 40px;
  height: 40px;
  margin-right: 20px;
`