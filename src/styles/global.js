import styled from 'styled-components';
import { Colors } from '../lib/theme';

export const Bold = styled.span`
  font-weight: bold
`
export const WhiteLText = styled.p`
  font-size: 30px;
  color: white;
  margin: 10px 0px;
  text-align: ${props => props.justify ? props.justify : 'left'};
`
export const WhiteMText = styled.p`
  font-size: 20px;
  color: white;
  text-align: ${props => props.justify ? props.justify : 'left'};
`
export const WhiteSText = styled.p`
  font-size: 14px;
  color: white;
  text-align: ${props => props.justify ? props.justify : 'left'};
`
export const LText = styled.p`
  font-size: 30px;
  color: ${Colors.text};
  margin: 10px 0px;
  text-align: ${props => props.justify ? props.justify : 'left'};
`
export const MText = styled.p`
  font-size: 20px;
  color: ${Colors.text};
  text-align: ${props => props.justify ? props.justify : 'left'};
`
export const SText = styled.p`
  font-size: 14px;
  color: ${Colors.text};
  text-align: ${props => props.justify ? props.justify : 'left'};
`
export const GrayLText = styled.p`
  font-size: 30px;
  color: ${Colors.gray};
  margin: 10px 0px;
  text-align: ${props => props.justify ? props.justify : 'left'};
`
export const GrayMText = styled.p`
  font-size: 20px;
  color: ${Colors.gray};
  text-align: ${props => props.justify ? props.justify : 'left'};
`
export const GraySText = styled.p`
  font-size: 14px;
  color: ${Colors.gray};
  text-align: ${props => props.justify ? props.justify : 'left'};
`
export const LightGrayLText = styled.p`
  font-size: 30px;
  color: ${Colors.lightgray};
  margin: 10px 0px; 
  text-align: ${props => props.justify ? props.justify : 'left'};
`
export const LightGrayMText = styled.p`
  font-size: 20px;
  color: ${Colors.lightgray};
  text-align: ${props => props.justify ? props.justify : 'left'};
`
export const LightGraySText = styled.p`
  font-size: 14px;
  color: ${Colors.lightgray};
  text-align: ${props => props.justify ? props.justify : 'left'};
`
export const RightView = styled.div`
  display: flex;
  padding: ${props => props.paddingTop ? props.paddingTop + 'px 0px' : '0'};
  justify-content: flex-end
`
export const WrapView = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  margin-top: 20px;
`
export const Row = styled.div`
  display: flex;
  align-items: center;
`
export const ReverseView = styled.div`
  display: flex;
  flex-direction: column-reverse;
`
export const CenterRow = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center
`
export const CenterColumn = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100%;
`
export const SBView = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: ${props => props.padding ? props.padding : '0'};
`
export const ErrorText = styled.p`
  color: ${Colors.error};
  font-size: 14px;
`

export const Avatar = styled.img`
  width
`